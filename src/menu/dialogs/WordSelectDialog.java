package menu.dialogs;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import menu.dialogs.data.Condition;
import menu.dialogs.data.DataManager;
import menu.dialogs.data.QuestionMaker;
import menu.dialogs.data.WordsList;
import menu.dialogs.gui.ImagePanel;

public class WordSelectDialog extends JFrame implements ActionListener {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	JButton btn_start;
	JLabel lbl_message;
	JPanel p_wordslist;
	WordsList wordslist;
	int level;

	public void actionPerformed(ActionEvent e){		//操作に対する動作を定義
		if(e.getSource()==this.btn_start){
			if(wordslist.start()){
				QuestionMaker.comfirm_question();
				DataManager.highscore = DataManager.LoadScore(level-1);
				MinaraiComfirmWordsDialog comfirmwordsf = new MinaraiComfirmWordsDialog(QuestionMaker.question,level);
				comfirmwordsf.setLocationRelativeTo(null);
				comfirmwordsf.setVisible(true);
				wordslist.dispose();
				this.dispose();
			}else{
				JOptionPane.showMessageDialog(null, "１つ以上の単語を選択してください");
			}
		}
	}
	
	public WordSelectDialog(int level){		//ボタンなどの用意、その他の初期設定
		
		//>>>>>> フレームパラメータ等のデフォルト値を設定 >>>>>>
		this.setTitle("みならいの魔法使い");
		this.setSize(800,600);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ImagePanel imagepanel = new ImagePanel("/menu/dialogs/gui/background.jpg");
		this.add(imagepanel);
		this.level = level;
		//<<<<<<
		
		//>>>>>> 部品の用意 >>>>>>
		this.lbl_message = new JLabel("ドラッグして単語を選択してください");
		this.lbl_message.setFont(new Font("HGP創英角ﾎﾟｯﾌﾟ体",Font.BOLD,30));
		lbl_message.setForeground(Color.ORANGE);
		
		String path="./dialogs_pics/comfirmIcon";
		this.btn_start = new JButton(new ImageIcon(path+".gif"));
		this.btn_start.addActionListener(this);
		this.btn_start.setContentAreaFilled(false);
		this.btn_start.setBorderPainted(false);
		this.btn_start.setRolloverIcon(new ImageIcon(path+"O.gif"));
		this.btn_start.setPressedIcon(new ImageIcon(path+"P.gif"));
		
		wordslist = new WordsList(level);
		p_wordslist = wordslist.getPanel();
		p_wordslist.setOpaque(false);
		
		//this.btn_start.setEnabled(false);
		//<<<<<<
		
		//>>>>>> レイアウト >>>>>>
		JPanel layer1p = new JPanel();
		layer1p.setOpaque(false);
		imagepanel.add(layer1p);
		layer1p.setLayout(new BoxLayout(layer1p,BoxLayout.Y_AXIS));
		
		JPanel layer2_0p = new JPanel();
		layer2_0p.setOpaque(false);
		layer2_0p.setLayout(new FlowLayout());
		
		JPanel layer2_1p = new JPanel();
		layer2_1p.setOpaque(false);
		layer2_1p.setLayout(new FlowLayout());
		
		JPanel layer2_2p = new JPanel();
		layer2_2p.setOpaque(false);
		layer2_2p.setLayout(new BoxLayout(layer2_2p,BoxLayout.X_AXIS));

		layer1p.add(Box.createRigidArea(new Dimension(50,10)));
		layer1p.add(layer2_0p);
		layer1p.add(Box.createRigidArea(new Dimension(300,10)));
		layer1p.add(layer2_1p);
		layer1p.add(Box.createRigidArea(new Dimension(50,10)));
		layer1p.add(layer2_2p);
		
		layer2_0p.add(lbl_message);
		layer2_1p.add(p_wordslist);
		layer2_2p.add(btn_start);
		//>>>>>>
	}
	
	public static void main(String[] args) {
		Condition.count_num();
		WordSelectDialog wordselect = new WordSelectDialog(1);
		wordselect.setLocationRelativeTo(null);
		wordselect.setVisible(true);
	}
}