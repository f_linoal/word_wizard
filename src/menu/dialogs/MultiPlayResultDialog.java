package menu.dialogs;
import game.telecommunication.Client;

import java.awt.event.ActionEvent;

public class MultiPlayResultDialog extends BoukenResultDialog {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void actionPerformed(ActionEvent e){		//操作に対する動作を定義
		if(e.getSource()==this.btn_playagain){
			NetworkPlayLevelSelectDialog networkplaylevelselectf = new NetworkPlayLevelSelectDialog();
			networkplaylevelselectf.setLocationRelativeTo(null);
			networkplaylevelselectf.setVisible(true);
			dispose();
		}
		if(e.getSource()==this.btn_back){
			ModeSelectDialog modeselectf = new ModeSelectDialog();
			modeselectf.setLocationRelativeTo(null);
			modeselectf.setVisible(true);
			dispose();
		}
		if(e.getSource()==this.btn_exit){
			System.exit(0);
		}
	}
	
	public void gameEnd(){
		Client.sendMessage("GameEnd");
	}

	public MultiPlayResultDialog(){
		super();
		gameEnd();
	}
}
