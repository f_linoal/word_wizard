package menu.dialogs;
import menu.dialogs.data.Condition;


	public class ResultDialog{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public ResultDialog(){
		if(Condition.playing_game_mode==1){
			MinaraiResultDialog resuletf = new MinaraiResultDialog();
			resuletf.setLocationRelativeTo(null);
			resuletf.setVisible(true);
		}
		
		if(Condition.playing_game_mode==2){
			KenkyuResultDialog resuletf = new KenkyuResultDialog();
			resuletf.setLocationRelativeTo(null);
			resuletf.setVisible(true);
		}
		
		if(Condition.playing_game_mode==3){
			BoukenResultDialog resuletf = new BoukenResultDialog();
			resuletf.setLocationRelativeTo(null);
			resuletf.setVisible(true);
		}
		
		if(Condition.playing_game_mode==4){
			MultiPlayResultDialog resuletf = new MultiPlayResultDialog();
			resuletf.setLocationRelativeTo(null);
			resuletf.setVisible(true);
		}
	}
}