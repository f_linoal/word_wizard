package menu.dialogs;
import java.awt.event.ActionEvent;

public class NetworkResultDialog extends BoukenResultDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void actionPerformed(ActionEvent e){		//操作に対する動作を定義
		if(e.getSource()==this.btn_playagain){
			this.setVisible(false);
			NetworkPlayLevelSelectDialog networkplaylevelselectf = new NetworkPlayLevelSelectDialog();
			networkplaylevelselectf.setLocationRelativeTo(null);
			networkplaylevelselectf.setVisible(true);
		}
		if(e.getSource()==this.btn_back){
			this.setVisible(false);
			ModeSelectDialog modeselectf = new ModeSelectDialog();
			modeselectf.setLocationRelativeTo(null);
			modeselectf.setVisible(true);
		}
		if(e.getSource()==this.btn_exit){
			System.exit(0);
		}
	}

	public NetworkResultDialog(){
		super();
	}
}
