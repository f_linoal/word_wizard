package menu.dialogs;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import menu.dialogs.data.Condition;
import menu.dialogs.data.DataManager;
import menu.dialogs.gui.ImagePanel;

public class NetworkPlayLevelSelectDialog extends JFrame implements ActionListener {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	JButton btn_level1;
	JButton btn_level2;
	JButton btn_level3;
	JButton btn_start;
	JButton btn_back;
	JLabel lbl_message;
	
	int level;

	public void actionPerformed(ActionEvent e){		//操作に対する動作を定義
		if(e.getSource()==this.btn_level1){
			level=1;
			btn_level1.setEnabled(false);
			btn_level2.setEnabled(true);
			btn_level3.setEnabled(true);
			this.btn_start.setEnabled(true);
		}
		if(e.getSource()==this.btn_level2){
			level=2;
			btn_level1.setEnabled(true);
			btn_level2.setEnabled(false);
			btn_level3.setEnabled(true);
			this.btn_start.setEnabled(true);
		}                         
		if(e.getSource()==this.btn_level3){
			level=3;
			btn_level1.setEnabled(true);
			btn_level2.setEnabled(true);
			btn_level3.setEnabled(false);
			this.btn_start.setEnabled(true);
		}
		if(e.getSource()==this.btn_start){
			MatchingDialog matchingf = new MatchingDialog(level);
			matchingf.setLocationRelativeTo(null);
			matchingf.setVisible(true);
			DataManager.PointReset();
			Condition.playing_game_level = level;
			Condition.playing_game_mode = 4;
			this.dispose();
		}
		if(e.getSource()==this.btn_back){
			ModeSelectDialog msf = new ModeSelectDialog();
			msf.setLocationRelativeTo(null);
			msf.setVisible(true);
			this.dispose();
		}
	}
	
	public NetworkPlayLevelSelectDialog(){		//ボタンなどの用意、その他の初期設定
		
		//>>>>>> フレームパラメータ等のデフォルト値を設定 >>>>>>
		this.setTitle("みならいの魔法使い");
		this.setSize(800,600);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ImagePanel imagepanel = new ImagePanel("/menu/dialogs/gui/background.jpg");
		this.add(imagepanel);
		//<<<<<<
		
		//>>>>>> 部品の用意 >>>>>>
		this.lbl_message = new JLabel("レベルを選択してスタートしてください");
		this.lbl_message.setFont(new Font("HGP創英角ﾎﾟｯﾌﾟ体",Font.BOLD,30));
		lbl_message.setForeground(Color.ORANGE);
		
		String path;
		path="./dialogs_pics/level1Icon";
		this.btn_level1 = new JButton(new ImageIcon(path+".gif"));
		this.btn_level1.addActionListener(this);
		this.btn_level1.setContentAreaFilled(false);
		this.btn_level1.setBorderPainted(false);
		this.btn_level1.setRolloverIcon(new ImageIcon(path+"O.gif"));
		this.btn_level1.setPressedIcon(new ImageIcon(path+"P.gif"));
		this.btn_level1.setDisabledIcon(new ImageIcon(path+"P.gif"));
		
		path="./dialogs_pics/level2Icon";
		this.btn_level2 = new JButton(new ImageIcon(path+".gif"));
		this.btn_level2.addActionListener(this);
		this.btn_level2.setContentAreaFilled(false);
		this.btn_level2.setBorderPainted(false);
		this.btn_level2.setRolloverIcon(new ImageIcon(path+"O.gif"));
		this.btn_level2.setPressedIcon(new ImageIcon(path+"P.gif"));
		this.btn_level2.setDisabledIcon(new ImageIcon(path+"P.gif"));
		
		path="./dialogs_pics/level3Icon";
		this.btn_level3 = new JButton(new ImageIcon(path+".gif"));
		this.btn_level3.addActionListener(this);
		this.btn_level3.setContentAreaFilled(false);
		this.btn_level3.setBorderPainted(false);
		this.btn_level3.setRolloverIcon(new ImageIcon(path+"O.gif"));
		this.btn_level3.setPressedIcon(new ImageIcon(path+"P.gif"));
		this.btn_level3.setDisabledIcon(new ImageIcon(path+"P.gif"));

		path="./dialogs_pics/startIcon";
		this.btn_start = new JButton(new ImageIcon(path+".gif"));
		this.btn_start.addActionListener(this);
		this.btn_start.setContentAreaFilled(false);
		this.btn_start.setBorderPainted(false);
		this.btn_start.setRolloverIcon(new ImageIcon(path+"O.gif"));
		this.btn_start.setPressedIcon(new ImageIcon(path+"P.gif"));
		this.btn_start.setEnabled(false);

		path="./dialogs_pics/backIcon";
		this.btn_back = new JButton(new ImageIcon(path+".gif"));
		this.btn_back.addActionListener(this);
		this.btn_back.setContentAreaFilled(false);
		this.btn_back.setBorderPainted(false);
		this.btn_back.setRolloverIcon(new ImageIcon(path+"O.gif"));
		this.btn_back.setPressedIcon(new ImageIcon(path+"P.gif"));
		//<<<<<<
		
		//>>>>>> レイアウト >>>>>>
		JPanel layer1p = new JPanel();
		layer1p.setOpaque(false);
		imagepanel.add(layer1p);
		layer1p.setLayout(new BoxLayout(layer1p,BoxLayout.Y_AXIS));
		
		JPanel layer2_0p = new JPanel();
		layer2_0p.setOpaque(false);
		layer2_0p.setLayout(new FlowLayout());
		
		JPanel layer2_1p = new JPanel();
		layer2_1p.setOpaque(false);
		layer2_1p.setLayout(new FlowLayout());
		
		JPanel layer2_2p = new JPanel();
		layer2_2p.setOpaque(false);
		layer2_2p.setLayout(new BoxLayout(layer2_2p,BoxLayout.X_AXIS));

		layer1p.add(Box.createRigidArea(new Dimension(50,80)));
		layer1p.add(layer2_0p);
		layer1p.add(Box.createRigidArea(new Dimension(300,230)));
		layer1p.add(layer2_1p);
		layer1p.add(Box.createRigidArea(new Dimension(50,30)));
		layer1p.add(layer2_2p);
		
		layer2_0p.add(lbl_message);
		layer2_1p.add(btn_level1);
		layer2_1p.add(btn_level2);
		layer2_1p.add(btn_level3);
		layer2_2p.add(btn_start);
		layer2_2p.add(btn_back);
		//>>>>>>
	}
}
