package menu.dialogs;
import game.dialogs.SinglePlayDialog;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import menu.dialogs.data.Condition;
import menu.dialogs.data.DataManager;
import menu.dialogs.data.QuestionMaker;
import menu.dialogs.gui.ImagePanel;

public class SingleLevelSelectDialog extends JFrame implements ActionListener {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	JButton btn_level1;
	JButton btn_level2;
	JButton btn_level3;
	JButton btn_wnum1;
	JButton btn_wnum2;
	JButton btn_wnum3;
	JButton btn_start;
	JButton btn_back;
	JLabel lbl_title;
	JLabel lbl_message;
	
	int level=-1,wnum=-1;

	public void actionPerformed(ActionEvent e){		//操作に対する動作を定義
		if(e.getSource()==this.btn_level1){
			level=1;
			btn_level1.setEnabled(false);
			btn_level2.setEnabled(true);
			btn_level3.setEnabled(true);
			ButtonEnable();
		}
		if(e.getSource()==this.btn_level2){
			level=2;
			btn_level1.setEnabled(true);
			btn_level2.setEnabled(false);
			btn_level3.setEnabled(true);
			ButtonEnable();
		}                         
		if(e.getSource()==this.btn_level3){
			level=3;
			btn_level1.setEnabled(true);
			btn_level2.setEnabled(true);
			btn_level3.setEnabled(false);
			ButtonEnable();
		}
		if(e.getSource()==this.btn_wnum1){
			wnum=10;
			btn_wnum1.setEnabled(false);
			btn_wnum2.setEnabled(true);
			btn_wnum3.setEnabled(true);
			ButtonEnable();
		}                          
		if(e.getSource()==this.btn_wnum2){ 
			wnum=30;
			btn_wnum1.setEnabled(true);
			btn_wnum2.setEnabled(false);
			btn_wnum3.setEnabled(true);
			ButtonEnable();
		}                          
		if(e.getSource()==this.btn_wnum3){	
			wnum=50;
			btn_wnum1.setEnabled(true);
			btn_wnum2.setEnabled(true);
			btn_wnum3.setEnabled(false);
			ButtonEnable();
		}
		if(e.getSource()==this.btn_start){
			QuestionMaker.Initializer(level, wnum);
			SinglePlayDialog singleplaydialog = new SinglePlayDialog(QuestionMaker.question);
			singleplaydialog.setLocationRelativeTo(null);
			DataManager.PointReset();
			Condition.playing_game_level = level;
			Condition.playing_game_mode = 3;
			DataManager.highscore = DataManager.LoadScore(level+5);
			dispose();
			
		}
		if(e.getSource()==this.btn_back){
			ModeSelectDialog msf = new ModeSelectDialog();
			msf.setLocationRelativeTo(null);
			msf.setVisible(true);
			dispose();
		}
	}
	
	public void ButtonEnable(){
		if(level!=-1&&wnum!=-1){
			btn_start.setEnabled(true);
		}
	}
	
	public SingleLevelSelectDialog(){		//ボタンなどの用意、その他の初期設定
		
		//>>>>>> フレームパラメータ等のデフォルト値を設定 >>>>>>
		this.setTitle("みならいの魔法使い");
		this.setSize(800,600);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ImagePanel imagepanel = new ImagePanel("/menu/dialogs/gui/background.jpg");
		this.add(imagepanel);
		//<<<<<<
		
		//>>>>>> 部品の用意 >>>>>>
		this.lbl_title = new JLabel(new ImageIcon("./dialogs_pics/bouken_logo.png"));
		
		this.lbl_message = new JLabel("レベルと単語数を選択してスタートしてください");
		this.lbl_message.setFont(new Font("HGP創英角ﾎﾟｯﾌﾟ体",Font.BOLD,30));
		this.lbl_message.setForeground(Color.ORANGE);
		
		String path;
		path="./dialogs_pics/level1Icon";
		this.btn_level1 = new JButton(new ImageIcon(path+".gif"));
		this.btn_level1.addActionListener(this);
		this.btn_level1.setContentAreaFilled(false);
		this.btn_level1.setBorderPainted(false);
		this.btn_level1.setRolloverIcon(new ImageIcon(path+"O.gif"));
		this.btn_level1.setPressedIcon(new ImageIcon(path+"P.gif"));
		this.btn_level1.setDisabledIcon(new ImageIcon(path+"P.gif"));
		
		path="./dialogs_pics/level2Icon";
		this.btn_level2 = new JButton(new ImageIcon(path+".gif"));
		this.btn_level2.addActionListener(this);
		this.btn_level2.setContentAreaFilled(false);
		this.btn_level2.setBorderPainted(false);
		this.btn_level2.setRolloverIcon(new ImageIcon(path+"O.gif"));
		this.btn_level2.setPressedIcon(new ImageIcon(path+"P.gif"));
		this.btn_level2.setDisabledIcon(new ImageIcon(path+"P.gif"));
		
		path="./dialogs_pics/level3Icon";
		this.btn_level3 = new JButton(new ImageIcon(path+".gif"));
		this.btn_level3.addActionListener(this);
		this.btn_level3.setContentAreaFilled(false);
		this.btn_level3.setBorderPainted(false);
		this.btn_level3.setRolloverIcon(new ImageIcon(path+"O.gif"));
		this.btn_level3.setPressedIcon(new ImageIcon(path+"P.gif"));
		this.btn_level3.setDisabledIcon(new ImageIcon(path+"P.gif"));
		
		path="./dialogs_pics/10wordsIcon";
		this.btn_wnum1 = new JButton(new ImageIcon(path+".gif"));

		this.btn_wnum1.addActionListener(this);
		this.btn_wnum1.setContentAreaFilled(false);
		this.btn_wnum1.setBorderPainted(false);
		this.btn_wnum1.setRolloverIcon(new ImageIcon(path+"O.gif"));
		this.btn_wnum1.setPressedIcon(new ImageIcon(path+"P.gif"));
		this.btn_wnum1.setDisabledIcon(new ImageIcon(path+"P.gif"));
		
		path="./dialogs_pics/30wordsIcon";
		this.btn_wnum2 = new JButton(new ImageIcon(path+".gif"));

		this.btn_wnum2.addActionListener(this);
		this.btn_wnum2.setContentAreaFilled(false);
		this.btn_wnum2.setBorderPainted(false);
		this.btn_wnum2.setRolloverIcon(new ImageIcon(path+"O.gif"));
		this.btn_wnum2.setPressedIcon(new ImageIcon(path+"P.gif"));
		this.btn_wnum2.setDisabledIcon(new ImageIcon(path+"P.gif"));
		
		path="./dialogs_pics/50wordsIcon";
		this.btn_wnum3 = new JButton(new ImageIcon(path+".gif"));
		this.btn_wnum3.addActionListener(this);
		this.btn_wnum3.setContentAreaFilled(false);
		this.btn_wnum3.setBorderPainted(false);
		this.btn_wnum3.setRolloverIcon(new ImageIcon(path+"O.gif"));
		this.btn_wnum3.setPressedIcon(new ImageIcon(path+"P.gif"));
		this.btn_wnum3.setDisabledIcon(new ImageIcon(path+"P.gif"));
		
		path="./dialogs_pics/startIcon";
		this.btn_start = new JButton(new ImageIcon(path+".gif"));
		this.btn_start.addActionListener(this);
		this.btn_start.setContentAreaFilled(false);
		this.btn_start.setBorderPainted(false);
		this.btn_start.setRolloverIcon(new ImageIcon(path+"O.gif"));
		this.btn_start.setPressedIcon(new ImageIcon(path+"P.gif"));
		this.btn_start.setEnabled(false);

		path="./dialogs_pics/backIcon";
		this.btn_back = new JButton(new ImageIcon(path+".gif"));
		this.btn_back.addActionListener(this);
		this.btn_back.setContentAreaFilled(false);
		this.btn_back.setBorderPainted(false);
		this.btn_back.setRolloverIcon(new ImageIcon(path+"O.gif"));
		this.btn_back.setPressedIcon(new ImageIcon(path+"P.gif"));
		//<<<<<<
		
		//>>>>>> レイアウト >>>>>>
		JPanel layer1p = new JPanel();
		layer1p.setOpaque(false);
		imagepanel.add(layer1p);
		layer1p.setLayout(new BoxLayout(layer1p,BoxLayout.Y_AXIS));
		
		JPanel layer2_m1p = new JPanel();
		layer2_m1p.setOpaque(false);
		layer2_m1p.setLayout(new FlowLayout());
		
		JPanel layer2_0p = new JPanel();
		layer2_0p.setOpaque(false);
		layer2_0p.setLayout(new FlowLayout());
		
		JPanel layer2_1p = new JPanel();
		layer2_1p.setOpaque(false);
		layer2_1p.setLayout(new FlowLayout());
		
		JPanel layer2_2p = new JPanel();
		layer2_2p.setOpaque(false);
		layer2_2p.setLayout(new FlowLayout());
		
		JPanel layer2_3p = new JPanel();
		layer2_3p.setOpaque(false);
		layer2_3p.setLayout(new BoxLayout(layer2_3p,BoxLayout.X_AXIS));

		layer1p.add(layer2_m1p);
		layer1p.add(Box.createRigidArea(new Dimension(50,80)));
		layer1p.add(layer2_0p);
		layer1p.add(Box.createRigidArea(new Dimension(300,50)));
		layer1p.add(layer2_1p);
		layer1p.add(Box.createRigidArea(new Dimension(50,10)));
		layer1p.add(layer2_2p);
		layer1p.add(Box.createRigidArea(new Dimension(50,30)));
		layer1p.add(layer2_3p);
		
		layer2_m1p.add(lbl_title);
		layer2_0p.add(lbl_message);
		layer2_1p.add(btn_level1);
		layer2_1p.add(btn_level2);
		layer2_1p.add(btn_level3);
		layer2_2p.add(btn_wnum1);
		layer2_2p.add(btn_wnum2);
		layer2_2p.add(btn_wnum3);
		layer2_3p.add(btn_start);
		layer2_3p.add(btn_back);
		//>>>>>>
		
	}
}
