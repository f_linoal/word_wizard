package menu.dialogs;
import java.awt.event.ActionEvent;

import javax.swing.ImageIcon;

import menu.dialogs.data.Condition;
import menu.dialogs.data.DataManager;
import menu.dialogs.data.QuestionMaker;


public class KenkyuLevelSelectDialog extends SingleLevelSelectDialog {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	int true_wnum;
	
	public void actionPerformed(ActionEvent e){		//操作に対する動作を定義
		if(e.getSource()==this.btn_level1){
			level=1;
			btn_level1.setEnabled(false);
			btn_level2.setEnabled(true);
			btn_level3.setEnabled(true);
			ButtonEnable();
		}
		if(e.getSource()==this.btn_level2){
			level=2;
			btn_level1.setEnabled(true);
			btn_level2.setEnabled(false);
			btn_level3.setEnabled(true);
			ButtonEnable();
		}                         
		if(e.getSource()==this.btn_level3){
			level=3;
			btn_level1.setEnabled(true);
			btn_level2.setEnabled(true);
			btn_level3.setEnabled(false);
			ButtonEnable();
		}
		if(e.getSource()==this.btn_wnum1){
			wnum=15;true_wnum=10;
			btn_wnum1.setEnabled(false);
			btn_wnum2.setEnabled(true);
			btn_wnum3.setEnabled(true);
			ButtonEnable();
		}                          
		if(e.getSource()==this.btn_wnum2){ 
			wnum=45;true_wnum=30;
			btn_wnum1.setEnabled(true);
			btn_wnum2.setEnabled(false);
			btn_wnum3.setEnabled(true);
			ButtonEnable();
		}                          
		if(e.getSource()==this.btn_wnum3){	
			wnum=70;true_wnum=50;
			btn_wnum1.setEnabled(true);
			btn_wnum2.setEnabled(true);
			btn_wnum3.setEnabled(false);
			ButtonEnable();
		}
		if(e.getSource()==this.btn_start){
			QuestionMaker.Initializer(level, wnum);
			KenkyuComfirmWordsDialog wordf = new KenkyuComfirmWordsDialog(QuestionMaker.question,level,true_wnum);
			wordf.setLocationRelativeTo(null);
			wordf.setVisible(true);
			DataManager.PointReset();
			Condition.playing_game_level = level;
			Condition.playing_game_mode = 2;
			DataManager.highscore = DataManager.LoadScore(level+2);
			this.dispose();
		}
		if(e.getSource()==this.btn_back){
			ModeSelectDialog msf = new ModeSelectDialog();
			msf.setLocationRelativeTo(null);
			msf.setVisible(true);
			this.dispose();
		}
	}
	
	public KenkyuLevelSelectDialog(){
		super();
		
		//>>>>>> 部品の用意 >>>>>>
		lbl_title.setIcon(new ImageIcon("./dialogs_pics/kenkyu_logo.png"));
		
		String path="./dialogs_pics/comfirmIcon";
		this.btn_start.setIcon(new ImageIcon(path+".gif"));
		this.btn_start.setRolloverIcon(new ImageIcon(path+"O.gif"));
		this.btn_start.setPressedIcon(new ImageIcon(path+"P.gif"));
		//this.btn_start.setEnabled(false);
		//>>>>>>
	}
}
