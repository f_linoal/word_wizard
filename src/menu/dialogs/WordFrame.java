package menu.dialogs;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

public class WordFrame extends JFrame implements ActionListener {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	JButton btn_start;

	public void actionPerformed(ActionEvent e){		//操作に対する動作を定義
		if(e.getSource()==this.btn_start){
			this.setVisible(false);
			KenkyuResultDialog resultdataf = new KenkyuResultDialog();
			resultdataf.setLocationRelativeTo(null);
			resultdataf.setVisible(true);
		}
	}
	
	public WordFrame(){		//ボタンなどの用意、その他の初期設定
		
		//>>>>>> フレームパラメータ等のデフォルト値を設定 >>>>>>
		this.setTitle("みならいの魔法使い");
		this.setSize(500,500);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLayout(new GridLayout(6,1));
		//<<<<<<
		
		//>>>>>> 部品の用意 >>>>>>
		this.btn_start = new JButton("スタート");
		this.btn_start.addActionListener(this);
		this.add(this.btn_start);
		//<<<<<<
		
	}
}