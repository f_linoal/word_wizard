package menu.dialogs.data;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.StringTokenizer;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.TableColumn;

public class WordsList extends JFrame{

  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

private String[][] tabledata;

  private String[] columnNames = {"No.", "English", "日本語"};
  
  JPanel p;
  JTable table;
  int level;

  public WordsList(int level){
	tabledata = new String[Condition.words_num_level[level]][5];  
    setTitle("WordsList");
    setBounds( 200, 200, 1000, 1000);

    this.level = level;
    gettabledata();
    
    table = new JTable(tabledata, columnNames);
    
    DefaultTableColumnModel columnModel = (DefaultTableColumnModel)table.getColumnModel();
    TableColumn column = null;
    column = columnModel.getColumn(0);
    column.setPreferredWidth(25);
    column = columnModel.getColumn(1);
    column.setPreferredWidth(120);
    column = columnModel.getColumn(2);
    column.setPreferredWidth(255);

    JScrollPane sp = new JScrollPane(table);
    sp.setPreferredSize(new Dimension(450, 400));

    p = new JPanel();
    p.add(sp);

    getContentPane().add(p, BorderLayout.CENTER);
  }
  
  public void gettabledata(){
	  try {
	      File csv = new File("Words.csv"); // CSVデータファイル
	      BufferedReader br = new BufferedReader(new FileReader(csv));
	      int i,j;
	      // 最終行まで読み込む
	      String line = "";
	      String [] buffer = new String[4];
	      i=0;
	      line = br.readLine();
	      while ((line = br.readLine()) != null) {
	    	  j=0;
		      StringTokenizer st = new StringTokenizer(line, ",");
    		  for(int n=0;n<4;n++){
    			  buffer[n]=st.nextToken();
    		  }
    		  if(Integer.parseInt(buffer[3])==level){
    		  tabledata[i][j]=buffer[0];
    		  tabledata[i][j+1]=buffer[1];
    		  tabledata[i][j+2]=buffer[2];
    		  i++;
    		  }
	      }
	      br.close();

	    } catch (Exception e) {
	      // Fileオブジェクト生成時の例外捕捉
	      e.printStackTrace();
	    }
  }
  
  public void makequestion(int words_num){
	  int[] selectedrows;
	  Question[] question;
	  question = new Question[words_num];
	  selectedrows = table.getSelectedRows();
	 // DefaultTableModel tableModel = (DefaultTableModel) table.getModel();
	  for(int i=0;i<words_num;i++){
		  //question[i] = new Question(tableModel.getValueAt(selectedrows[i], 1).toString(),tableModel.getValueAt(selectedrows[i], 1).toString());
		  question[i] = new Question((String)table.getValueAt(selectedrows[i],1),(String)table.getValueAt(selectedrows[i],2));
	  }
	  QuestionMaker.set_question(question,words_num);
  }
  
  public boolean start(){
	  int selectedrows_num;
	  selectedrows_num = table.getSelectedRowCount();
	  if(selectedrows_num==0){
		  return false;
	  }else
		  makequestion(selectedrows_num);
		  return true;
  }
  
  public JPanel getPanel(){
	  return p;
  }
  
  public static void main(String[] args){
	  WordsList test = new WordsList(1);

    test.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    test.setVisible(true);
  }
}