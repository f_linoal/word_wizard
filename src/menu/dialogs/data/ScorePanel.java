package menu.dialogs.data;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Image;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class ScorePanel extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	int score;
	JPanel panel,numberpanel;
	JLabel lbl_num[];
	ImageIcon numIcon[];
	JPanel panel2;
	
	public ScorePanel(int score,double bairitu){
		this.score = score;
		
		//>>>>>> フレームパラメータ等のデフォルト値を設定 >>>>>>
		this.setTitle("Score");
		this.setSize(800,600);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//<<<<<<
		
		numIcon = new ImageIcon[10];
		ImageIcon BufferIcon;
		Image [] numImage;
		numImage = new Image[10];
		for(int i=0;i<10;i++){
			BufferIcon = new ImageIcon("./dialogs_pics/"+ i + "Icon.png");
			numImage[i] = BufferIcon.getImage().getScaledInstance((int) (BufferIcon.getIconWidth() * bairitu), -1,Image.SCALE_SMOOTH);
			numIcon[i] = new ImageIcon(numImage[i]);
		}
		
		panel = new JPanel();
		//panel.setLayout(new BoxLayout(panel,BoxLayout.LINE_AXIS));		
		panel.setLayout(new FlowLayout());
		panel.setOpaque(false);
		setNumPics(score);
		
		panel2 = new JPanel();
		panel2.setLayout(new BoxLayout(panel2,BoxLayout.Y_AXIS));
		panel2.setOpaque(false);
		panel2.add((Box.createRigidArea(new Dimension(5,13))));
		panel2.add(panel);
		
		//getContentPane().add(panel2, BorderLayout.CENTER);
	}
	
	public JPanel getNumPanel(){
		return panel2;
	}
	
	private void setNumPics(int num){
		int buffer_keta,keta=0,buffer1,buffer2;
		int [] number;
		keta = Integer.toString(num).length();
		number = new int [keta];
		buffer_keta = keta;
		int i=0,j;
		while(buffer_keta>0){
			buffer1 = num;
			buffer2 = num;
			j=i;
			while(j>0){
				buffer1 = buffer1/10;
				buffer2 = buffer2/10;
				j--;
			}
			buffer1 = buffer1/10;
			buffer1 = buffer1*10;
			number[buffer_keta-1] = buffer2-buffer1;
			i++;
			buffer_keta--;
		}
		lbl_num = new JLabel[keta];
		for(i=0;i<keta;i++){
			lbl_num[i] = new JLabel(numIcon[number[i]]);
			panel.add(lbl_num[i]);
		}
	}
	
	public static void main(String[] args){
		ScorePanel scorep = new ScorePanel(123456,1);
		scorep.setLocationRelativeTo(null);
		scorep.setVisible(true);
	}
}
