package menu.dialogs.data;

import java.io.*;
import java.util.*;

public class QuestionMaker {
	static int word_level,word_num;
	public static Question [] prequestion,question;
	static boolean [] extraction_num,randomflag;
	public static int question_num;
	
	public static void Initializer(int level,int num){
		word_level = level;
		word_num = num;
		prequestion = new Question[num];
		question = new Question[num];
		randomflag = new boolean[num];
		maker();
	}
	
	public static void maker(){
		try {
			select_extraction_num();
			File csv = new File("Words.csv");
			BufferedReader br = new BufferedReader(new FileReader(csv));
			String line = "";
			int i=0,j=0;
			while ((line = br.readLine()) != null) {
				StringTokenizer st = new StringTokenizer(line, ",");
				if(extraction_num[i]){
					while (st.hasMoreTokens()) {
						st.nextToken();
						prequestion[j] = new Question(st.nextToken(),st.nextToken());
						st.nextToken();
						j++;
					}
				}
				i++;
			}
			br.close();
				
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}	
		word_random();
	}
	
	public static void select_extraction_num(){
		Random rnd = new Random();
		extraction_num = new boolean[Condition.all_words_num+1];
		int i=0,random_num;
		do{
			random_num = rnd.nextInt(Condition.all_words_num)+1;
			if(extraction_num[random_num]!=true){
				extraction_num[random_num] = true;
				i++;
			}
		}while(i<word_num);
	}

	public static void word_random(){
		Random rnd = new Random();
		int i=0,random_num;
		do{
			random_num = rnd.nextInt(word_num);
			if(randomflag[random_num]!=true){
				randomflag[random_num]=true;
				question[i]=prequestion[random_num];
				i++;
			}
		}while(i<word_num);
	}
	
	public static void kenkyuMaker(Question[] q,int word_num){
		Random rnd = new Random();
		int i=0,random_num;
		randomflag = new boolean[q.length];
		question = new Question[word_num];
		do{
			random_num = rnd.nextInt(q.length);
			if(randomflag[random_num]!=true){
				randomflag[random_num]=true;
				question[i]=q[random_num];
				i++;
			}
		}while(i<word_num);
	}
	
	public static void set_question(Question[] q,int n){
		question = q;
		question_num = n;
	}
	
	public static void comfirm_question(){
		for(int i=0;i<question.length;i++){
			System.out.println(question[i].question +"  "+question[i].answer);
		}
	}
	
	public static void main(String[] args){
		Condition.count_num();
		QuestionMaker.Initializer(1,10);
		/*Question[] q = QuestionMaker.question;
		for(int i=0;i<10;i++){
			System.out.println(q[i].question +"  "+q[i].answer);
		}*/
		QuestionMaker.comfirm_question();
	}
}
