package menu.dialogs.data;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;

public class DataManager {
	
	public static int score;
	private static FileReader inFile;
	private static FileWriter outFile;
	private static BufferedReader inBuffer;
	private static BufferedWriter outBuffer;
	public static int highscore_array[] = {0,0,0,0,0,0,0,0,0};
	public static int highscore;
	public static int win_flag;		//負け:0  勝ち:1  引き分け:2
	
////////////////////外部から使用するメソッド/////////////////////////////////////////////
	
	public static void PointReset(){
		score = 0;
	}
	
	public static void PointIncreace(int point){	//（引数）得点を加算する
		score = score + point;
	}

	public static void SaveScore(){		//highscore.txtに現在のハイスコアを保存する
		try{
			outFile = new FileWriter("highscore.txt");
			outBuffer = new BufferedWriter(outFile);
			for(int i=0;i<9;i++){
				outBuffer.write(""+highscore_array[i]+"\n");
			}
			outBuffer.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public static int LoadScore(int level){		//引数レベルのスコアを返す
		
		String line;
		int i;
		
		try{
			inFile = new FileReader("highscore.txt");
			inBuffer = new BufferedReader(inFile);
			i=0;
			while((line = inBuffer.readLine()) != null){
				highscore_array[i] = Integer.parseInt(line);
				//System.out.println(""+highscore[i]);
				i++;
			}
			inBuffer.close();
		}catch (Exception e){
			e.printStackTrace();
		}
		return highscore_array[level];
	}
	
	public static void ResetHighscore(){	//ハイスコアを全てリセットする
		
		for(int i=0;i<9;i++){
			highscore_array[i]=0;
		}
		SaveScore();
	}
	
	public static boolean setHighScore(){
		int level=-1;
		LoadScore(1);
		if(Condition.playing_game_mode==4){
			return false;
		}else{
			if(Condition.playing_game_mode==1){
				level = Condition.playing_game_level-1;
			}
			if(Condition.playing_game_mode==2){
				level = Condition.playing_game_level+2;
			}
			if(Condition.playing_game_mode==3){
				level = Condition.playing_game_level+5;
			}
			if(score>highscore_array[level]){
				highscore_array[level]=score;
				SaveScore();
				return true;
			}else{
				return false;
			}
		}
	}
	
/////////////////////////////////////////////////////////////////////////////////

	
}
