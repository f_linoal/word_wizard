package menu.dialogs.data;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.TableColumn;

public class ComfirmWordsList extends JFrame{

  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

private String[][] tabledata;

  private String[] columnNames = {"English", "���{��"};
  
  JPanel p;
  JTable table;
  Question[] question;

  public ComfirmWordsList(Question[] question){
	tabledata = new String[question.length][2];  
    
    this.question = question;

    gettabledata();
    
    table = new JTable(tabledata, columnNames);
    
    DefaultTableColumnModel columnModel = (DefaultTableColumnModel)table.getColumnModel();
    TableColumn column = null;
    
    column = columnModel.getColumn(0);
    column.setPreferredWidth(120);
    column = columnModel.getColumn(1);
    column.setPreferredWidth(260);

    JScrollPane sp = new JScrollPane(table);
    sp.setPreferredSize(new Dimension(380, 400));

    p = new JPanel();
    p.add(sp);

    getContentPane().add(p, BorderLayout.CENTER);
  }
  
  public void gettabledata(){
      int i;
      i=0;
      for(i=0;i<question.length;i++){
    	  tabledata[i][0]=question[i].answer;
    	  tabledata[i][1]=question[i].question;
      }
  }
  
  public JPanel getPanel(){
	  return p;
  }
}
