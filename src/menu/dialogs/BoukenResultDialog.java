package menu.dialogs;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import menu.dialogs.data.Condition;
import menu.dialogs.data.DataManager;
import menu.dialogs.data.ScorePanel;
import menu.dialogs.gui.ImagePanel;

public class BoukenResultDialog extends JFrame implements ActionListener {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	JButton btn_playagain;
	JButton btn_back;
	JButton btn_exit;
	JLabel lbl_title;
	JLabel lbl_message;
	JPanel scorepanel;

	public void actionPerformed(ActionEvent e){		//操作に対する動作を定義
		if(e.getSource()==this.btn_playagain){
			this.setVisible(false);
			SingleLevelSelectDialog singlelevelselectf = new SingleLevelSelectDialog();
			singlelevelselectf.setLocationRelativeTo(null);
			singlelevelselectf.setVisible(true);
		}
		if(e.getSource()==this.btn_back){
			this.setVisible(false);
			ModeSelectDialog modeselectf = new ModeSelectDialog();
			modeselectf.setLocationRelativeTo(null);
			modeselectf.setVisible(true);
		}
		if(e.getSource()==this.btn_exit){
			System.exit(0);
		}
	}
	
	public BoukenResultDialog(){		//ボタンなどの用意、その他の初期設定
		
		//>>>>>> フレームパラメータ等のデフォルト値を設定 >>>>>>
		this.setTitle("みならいの魔法使い");
		this.setSize(800,600);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ImagePanel imagepanel = new ImagePanel("/menu/dialogs/gui/background.jpg");
		this.add(imagepanel);
		setVisible(true);
		//<<<<<<
		
		//>>>>>> 部品の用意 >>>>>>
		this.lbl_title = new JLabel(new ImageIcon("./dialogs_pics/kekka_logo.png"));
		
		this.lbl_message = new JLabel("New HighScore!!");
		this.lbl_message.setFont(new Font("HGP創英角ﾎﾟｯﾌﾟ体",Font.BOLD,30));
		lbl_message.setForeground(Color.ORANGE);
		ScorePanel spanel = new ScorePanel(DataManager.score,1);
		scorepanel = spanel.getNumPanel();
		scorepanel.setOpaque(false);
		
		String path;
		path="./dialogs_pics/againIcon";
		this.btn_playagain = new JButton(new ImageIcon(path+".gif"));
		this.btn_playagain.addActionListener(this);
		this.btn_playagain.setContentAreaFilled(false);
		this.btn_playagain.setBorderPainted(false);
		this.btn_playagain.setRolloverIcon(new ImageIcon(path+"O.gif"));
		this.btn_playagain.setPressedIcon(new ImageIcon(path+"P.gif"));
		this.btn_playagain.setDisabledIcon(new ImageIcon(path+"P.gif"));
		
		path="./dialogs_pics/modeselectIcon";
		this.btn_back = new JButton(new ImageIcon(path+".gif"));
		this.btn_back.addActionListener(this);
		this.btn_back.setContentAreaFilled(false);
		this.btn_back.setBorderPainted(false);
		this.btn_back.setRolloverIcon(new ImageIcon(path+"O.gif"));
		this.btn_back.setPressedIcon(new ImageIcon(path+"P.gif"));
		this.btn_back.setDisabledIcon(new ImageIcon(path+"P.gif"));

		path="./dialogs_pics/exitIcon";
		this.btn_exit = new JButton(new ImageIcon(path+".gif"));
		this.btn_exit.addActionListener(this);
		this.btn_exit.setContentAreaFilled(false);
		this.btn_exit.setBorderPainted(false);
		this.btn_exit.setRolloverIcon(new ImageIcon(path+"O.gif"));
		this.btn_exit.setPressedIcon(new ImageIcon(path+"P.gif"));
		//<<<<<<
		
		//>>>>>> レイアウト >>>>>>
		JPanel layer1p = new JPanel();
		layer1p.setOpaque(false);
		imagepanel.add(layer1p);
		layer1p.setLayout(new BoxLayout(layer1p,BoxLayout.Y_AXIS));
		
		JPanel layer2_0p = new JPanel();
		layer2_0p.setOpaque(false);
		layer2_0p.setLayout(new FlowLayout());
		
		JPanel layer2_01p = new JPanel();
		layer2_01p.setOpaque(false);
		layer2_01p.setLayout(new FlowLayout());
		
		JPanel layer2_1p = new JPanel();
		layer2_1p.setOpaque(false);
		layer2_1p.setLayout(new FlowLayout());
		
		JPanel layer2_2p = new JPanel();
		layer2_2p.setOpaque(false);
		layer2_2p.setLayout(new BoxLayout(layer2_2p,BoxLayout.X_AXIS));
		if(DataManager.setHighScore()){
			layer1p.add(layer2_0p);
			layer1p.add(Box.createRigidArea(new Dimension(300,80)));
			layer1p.add(layer2_01p);
			layer1p.add(layer2_1p);
			layer1p.add(Box.createRigidArea(new Dimension(50,90)));
			layer1p.add(layer2_2p);
			
			layer2_0p.add(lbl_title);
			layer2_01p.add(lbl_message);
			layer2_1p.add(scorepanel);
			layer2_2p.add(btn_playagain);
			layer2_2p.add(btn_back);
			layer2_2p.add(btn_exit);
		}else if(Condition.playing_game_mode == 4){
			JLabel lbl_win = new JLabel();
			if(DataManager.win_flag==0){
				lbl_win.setIcon(new ImageIcon("./dialogs_pics/loseIcon.png"));
			}
			if(DataManager.win_flag==1){
				lbl_win.setIcon(new ImageIcon("./dialogs_pics/wonIcon.png"));
			}
			if(DataManager.win_flag==2){
				lbl_win.setIcon(new ImageIcon("./dialogs_pics/drawIcon.png"));
			}
			layer1p.add(layer2_0p);
			layer1p.add(Box.createRigidArea(new Dimension(300,110)));
			layer1p.add(layer2_1p);
			layer1p.add(Box.createRigidArea(new Dimension(50,90)));
			layer1p.add(layer2_2p);
			
			layer2_0p.add(lbl_title);
			layer2_1p.add(lbl_win);
			layer2_2p.add(btn_playagain);
			layer2_2p.add(btn_back);
			layer2_2p.add(btn_exit);
		}else{
			layer1p.add(layer2_0p);
			layer1p.add(Box.createRigidArea(new Dimension(300,110)));
			layer1p.add(layer2_1p);
			layer1p.add(Box.createRigidArea(new Dimension(50,90)));
			layer1p.add(layer2_2p);
			
			layer2_0p.add(lbl_title);
			layer2_1p.add(scorepanel);
			layer2_2p.add(btn_playagain);
			layer2_2p.add(btn_back);
			layer2_2p.add(btn_exit);
		}
		//>>>>>>
		
	}
	
	public static void main(String[] args) {
		BoukenResultDialog resultdialog = new BoukenResultDialog();
		resultdialog.setLocationRelativeTo(null);
		resultdialog.setVisible(true);
		Condition.count_num();
	}
}