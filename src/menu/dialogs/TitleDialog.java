package menu.dialogs;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import menu.dialogs.gui.ImagePanel;

public class TitleDialog extends JFrame implements ActionListener {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	JButton btn_start;
	JLabel lbl_logo;

	public void actionPerformed(ActionEvent e){		//操作に対する動作を定義
		if(e.getSource()==this.btn_start){
			ModeSelectDialog modeselectf = new ModeSelectDialog();
			modeselectf.setLocationRelativeTo(null);
			modeselectf.setVisible(true);
			this.dispose();
		}
	}
	
	public TitleDialog(){		//ボタンなどの用意、その他の初期設定
		
		//>>>>>> フレームパラメータ等のデフォルト値を設定 >>>>>>
		this.setTitle("みならいの魔法使い");
		this.setSize(800,600);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ImagePanel imagepanel = new ImagePanel("/menu/dialogs/gui/background.jpg");
		this.add(imagepanel);
		//<<<<<<
		
		//>>>>>> 部品の用意 >>>>>>
		String path;
		
		path="./dialogs_pics/startIcon";
		this.btn_start = new JButton(new ImageIcon(path+".gif"));
		this.btn_start.addActionListener(this);
		btn_start.setContentAreaFilled(false);
		btn_start.setBorderPainted(false);
		btn_start.setRolloverIcon(new ImageIcon(path+"O.gif"));
		btn_start.setPressedIcon(new ImageIcon(path+"P.gif"));  
		
		lbl_logo = new JLabel();
		lbl_logo.setIcon(new ImageIcon("./dialogs_pics/logo.png"));
		//<<<<<<
		
		//>>>>>> レイアウト>>>>>>
		JPanel layer1p = new JPanel();
		layer1p.setOpaque(false);
		imagepanel.add(layer1p);
		layer1p.setLayout(new BoxLayout(layer1p,BoxLayout.Y_AXIS));
		
		JPanel layer2_0p = new JPanel();
		layer2_0p.setOpaque(false);
		layer2_0p.setLayout(new FlowLayout());
		
		JPanel layer2_1p = new JPanel();
		layer2_1p.setOpaque(false);
		layer2_1p.setLayout(new FlowLayout());
		
		JPanel layer2_2p = new JPanel();
		layer2_2p.setOpaque(false);
		layer2_2p.setLayout(new BoxLayout(layer2_2p,BoxLayout.X_AXIS));

		layer1p.add(Box.createRigidArea(new Dimension(5,150)));
		layer1p.add(layer2_0p);
		layer1p.add(Box.createRigidArea(new Dimension(300,130)));
		layer1p.add(layer2_1p);
		layer1p.add(layer2_2p);
		
		layer2_0p.add(lbl_logo);
		layer2_2p.add(Box.createRigidArea(new Dimension(5,90)));
		layer2_2p.add(this.btn_start);		
		//>>>>>> 
		
		
		
	}
}
