package menu.dialogs;
import java.awt.event.ActionEvent;

public class MinaraiResultDialog extends BoukenResultDialog {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void actionPerformed(ActionEvent e){		//操作に対する動作を定義
		if(e.getSource()==this.btn_playagain){
			MinaraiLevelSelectDialog minarailevelselectf = new MinaraiLevelSelectDialog();
			minarailevelselectf.setLocationRelativeTo(null);
			minarailevelselectf.setVisible(true);
			dispose();
		}
		if(e.getSource()==this.btn_back){
			ModeSelectDialog modeselectf = new ModeSelectDialog();
			modeselectf.setLocationRelativeTo(null);
			modeselectf.setVisible(true);
			dispose();
		}
		if(e.getSource()==this.btn_exit){
			System.exit(0);
		}
	}
	
	public MinaraiResultDialog(){
		super();
	}
}
