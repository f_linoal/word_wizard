package menu.dialogs;
import game.dialogs.SinglePlayDialog;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import menu.dialogs.data.ComfirmWordsList;
import menu.dialogs.data.Condition;
import menu.dialogs.data.DataManager;
import menu.dialogs.data.Question;
import menu.dialogs.data.QuestionMaker;
import menu.dialogs.gui.ImagePanel;

public class MinaraiComfirmWordsDialog extends JFrame implements ActionListener {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	JButton btn_start,btn_back;
	JLabel lbl_message;
	JPanel p_comfirmwordslist;
	ComfirmWordsList comfirmwordslist;
	Question[] question;
	int level;

	public void actionPerformed(ActionEvent e){		//操作に対する動作を定義
		if(e.getSource()==this.btn_start){
			DataManager.PointReset();
			SinglePlayDialog singleplaydialog = new SinglePlayDialog(QuestionMaker.question);
			singleplaydialog.setLocationRelativeTo(null);
			comfirmwordslist.dispose();
			this.dispose();
		}
		if(e.getSource()==this.btn_back){
			WordSelectDialog wordf = new WordSelectDialog(level);
			wordf.setLocationRelativeTo(null);
			wordf.setVisible(true);
			comfirmwordslist.dispose();
			this.dispose();
		}
	}
	
	public MinaraiComfirmWordsDialog(Question[] question,int level){		//ボタンなどの用意、その他の初期設定
		
		//>>>>>> フレームパラメータ等のデフォルト値を設定 >>>>>>
		this.setTitle("みならいの魔法使い");
		this.setSize(800,600);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ImagePanel imagepanel = new ImagePanel("/menu/dialogs/gui/background.jpg");
		this.add(imagepanel);
		this.level = level;
		//<<<<<<
		
		//>>>>>> 部品の用意 >>>>>>
		this.lbl_message = new JLabel("以下の単語でゲームを始めます");
		this.lbl_message.setFont(new Font("HGP創英角ﾎﾟｯﾌﾟ体",Font.BOLD,30));
		lbl_message.setForeground(Color.ORANGE);
		
		String path="./dialogs_pics/backIcon";
		this.btn_back = new JButton(new ImageIcon(path+".gif"));
		this.btn_back.addActionListener(this);
		this.btn_back.setContentAreaFilled(false);
		this.btn_back.setBorderPainted(false);
		this.btn_back.setRolloverIcon(new ImageIcon(path+"O.gif"));
		this.btn_back.setPressedIcon(new ImageIcon(path+"P.gif"));
		
		path="./dialogs_pics/startIcon";
		this.btn_start = new JButton(new ImageIcon(path+".gif"));
		this.btn_start.addActionListener(this);
		this.btn_start.setContentAreaFilled(false);
		this.btn_start.setBorderPainted(false);
		this.btn_start.setRolloverIcon(new ImageIcon(path+"O.gif"));
		this.btn_start.setPressedIcon(new ImageIcon(path+"P.gif"));
		
		comfirmwordslist = new ComfirmWordsList(question);
		p_comfirmwordslist = comfirmwordslist.getPanel();
		p_comfirmwordslist.setOpaque(false);
		
		//this.btn_start.setEnabled(false);
		//<<<<<<
		
		//>>>>>> レイアウト >>>>>>
		JPanel layer1p = new JPanel();
		layer1p.setOpaque(false);
		imagepanel.add(layer1p);
		layer1p.setLayout(new BoxLayout(layer1p,BoxLayout.Y_AXIS));
		
		JPanel layer2_0p = new JPanel();
		layer2_0p.setOpaque(false);
		layer2_0p.setLayout(new FlowLayout());
		
		JPanel layer2_1p = new JPanel();
		layer2_1p.setOpaque(false);
		layer2_1p.setLayout(new FlowLayout());
		
		JPanel layer2_2p = new JPanel();
		layer2_2p.setOpaque(false);
		layer2_2p.setLayout(new BoxLayout(layer2_2p,BoxLayout.X_AXIS));

		layer1p.add(Box.createRigidArea(new Dimension(50,10)));
		layer1p.add(layer2_0p);
		layer1p.add(Box.createRigidArea(new Dimension(300,10)));
		layer1p.add(layer2_1p);
		layer1p.add(Box.createRigidArea(new Dimension(50,10)));
		layer1p.add(layer2_2p);
		
		layer2_0p.add(lbl_message);
		layer2_1p.add(p_comfirmwordslist);
		layer2_2p.add(btn_start);
		layer2_2p.add(btn_back);
		//>>>>>>
	}
	
	public static void main(String[] args) {
		WordSelectDialog wordselect = new WordSelectDialog(1);
		wordselect.setLocationRelativeTo(null);
		wordselect.setVisible(true);
		Condition.count_num();
	}
}