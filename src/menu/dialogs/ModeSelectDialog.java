package menu.dialogs;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import menu.dialogs.gui.ImagePanel;

public class ModeSelectDialog extends JFrame implements ActionListener {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	JButton btn_minarai_mode;
	JButton btn_kenkyu_mode;
	JButton btn_bouken_mode;
	JButton btn_taisen_mode;
	JButton btn_senseki;
	JButton btn_exit;
	JLabel lbl_logo;

	public void actionPerformed(ActionEvent e){		//操作に対する動作を定義
		if(e.getSource()==this.btn_minarai_mode){
			MinaraiLevelSelectDialog minarailevelselectf = new MinaraiLevelSelectDialog();
			minarailevelselectf.setLocationRelativeTo(null);
			minarailevelselectf.setVisible(true);
			this.dispose();
		}
		if(e.getSource()==this.btn_kenkyu_mode){
			KenkyuLevelSelectDialog kenkyulevelselectf = new KenkyuLevelSelectDialog();
			kenkyulevelselectf.setLocationRelativeTo(null);
			kenkyulevelselectf.setVisible(true);
			this.dispose();
		}
		if(e.getSource()==this.btn_bouken_mode){
			SingleLevelSelectDialog singlelevelselectf = new SingleLevelSelectDialog();
			singlelevelselectf.setLocationRelativeTo(null);
			singlelevelselectf.setVisible(true);
			this.dispose();
		}
		if(e.getSource()==this.btn_taisen_mode){
			NetworkPlayLevelSelectDialog networkplaylevelselectf = new NetworkPlayLevelSelectDialog();
			networkplaylevelselectf.setLocationRelativeTo(null);
			networkplaylevelselectf.setVisible(true);
			this.dispose();
		}
		if(e.getSource()==this.btn_senseki){
			RecordDialog recordf = new RecordDialog();
			recordf.setLocationRelativeTo(null);
			recordf.setVisible(true);
			this.dispose();
		}
		if(e.getSource()==this.btn_exit){
			System.exit(0);
		}
	}
	
	public ModeSelectDialog(){		//ボタンなどの用意、その他の初期設定
		
		//>>>>>> フレームパラメータ等のデフォルト値を設定 >>>>>>
		this.setTitle("みならいの魔法使い");
		this.setSize(800,600);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ImagePanel imagepanel = new ImagePanel("/menu/dialogs/gui/background.jpg");
		this.add(imagepanel);
		//<<<<<<
		
		//>>>>>> 部品の用意 >>>>>>
		String path;
		
		path="./dialogs_pics/minaraiIcon";
		this.btn_minarai_mode = new JButton(new ImageIcon(path+".gif"));
		this.btn_minarai_mode.addActionListener(this);
		btn_minarai_mode.setContentAreaFilled(false);
	    btn_minarai_mode.setBorderPainted(false);
	    btn_minarai_mode.setRolloverIcon(new ImageIcon(path+"O.gif"));
	    btn_minarai_mode.setPressedIcon(new ImageIcon(path+"P.gif"));  
		
	    path="./dialogs_pics/kenkyuIcon";
		this.btn_kenkyu_mode = new JButton(new ImageIcon(path+".gif"));
		this.btn_kenkyu_mode.addActionListener(this);
		btn_kenkyu_mode.setContentAreaFilled(false);
	    btn_kenkyu_mode.setBorderPainted(false);
	    btn_kenkyu_mode.setRolloverIcon(new ImageIcon(path+"O.gif"));
	    btn_kenkyu_mode.setPressedIcon(new ImageIcon(path+"P.gif"));  
		
	    path="./dialogs_pics/boukenIcon";
		this.btn_bouken_mode = new JButton(new ImageIcon(path+".gif"));
		this.btn_bouken_mode.addActionListener(this);
		btn_bouken_mode.setContentAreaFilled(false);
	    btn_bouken_mode.setBorderPainted(false);
	    btn_bouken_mode.setRolloverIcon(new ImageIcon(path+"O.gif"));
	    btn_bouken_mode.setPressedIcon(new ImageIcon(path+"P.gif"));  
		
		path="./dialogs_pics/taisenIcon";                                 
		this.btn_taisen_mode = new JButton(new ImageIcon(path+".gif"));   
		this.btn_taisen_mode.addActionListener(this);                     
		btn_taisen_mode.setContentAreaFilled(false);                      
		btn_taisen_mode.setBorderPainted(false);                          
		btn_taisen_mode.setRolloverIcon(new ImageIcon(path+"O.gif"));     
		btn_taisen_mode.setPressedIcon(new ImageIcon(path+"P.gif"));      

		path="./dialogs_pics/sensekiIcon";                                 
		this.btn_senseki = new JButton(new ImageIcon(path+".gif"));   
		this.btn_senseki.addActionListener(this);                     
		btn_senseki.setContentAreaFilled(false);                      
		btn_senseki.setBorderPainted(false);                          
		btn_senseki.setRolloverIcon(new ImageIcon(path+"O.gif"));     
		btn_senseki.setPressedIcon(new ImageIcon(path+"P.gif"));      
		
		path="./dialogs_pics/exitIcon";                                 
		this.btn_exit = new JButton(new ImageIcon(path+".gif"));   
		this.btn_exit.addActionListener(this);                     
		btn_exit.setContentAreaFilled(false);                      
		btn_exit.setBorderPainted(false);                          
		btn_exit.setRolloverIcon(new ImageIcon(path+"O.gif"));     
		btn_exit.setPressedIcon(new ImageIcon(path+"P.gif"));      
		
		lbl_logo = new JLabel();
		lbl_logo.setIcon(new ImageIcon("./dialogs_pics/logo.png"));
		//<<<<<<
		
		//>>>>>> レイアウト>>>>>>
		JPanel layer1p = new JPanel();
		layer1p.setOpaque(false);
		imagepanel.add(layer1p);
		layer1p.setLayout(new BoxLayout(layer1p,BoxLayout.Y_AXIS));
		
		JPanel layer2_0p = new JPanel();
		layer2_0p.setOpaque(false);
		layer2_0p.setLayout(new FlowLayout());
		
		JPanel layer2_1p = new JPanel();
		layer2_1p.setOpaque(false);
		layer2_1p.setLayout(new FlowLayout());
		
		JPanel layer2_2p = new JPanel();
		layer2_2p.setOpaque(false);
		layer2_2p.setLayout(new BoxLayout(layer2_2p,BoxLayout.X_AXIS));

		
		layer1p.add(layer2_0p);
		layer1p.add(Box.createRigidArea(new Dimension(300,230)));
		layer1p.add(layer2_1p);
		layer1p.add(layer2_2p);

		
		layer2_0p.add(lbl_logo);
		layer2_1p.add(this.btn_minarai_mode);
		layer2_1p.add(this.btn_kenkyu_mode);
		layer2_1p.add(this.btn_bouken_mode);
		layer2_1p.add(this.btn_taisen_mode);
		layer2_2p.add(this.btn_senseki);
		layer2_2p.add(Box.createRigidArea(new Dimension(5,90)));
		layer2_2p.add(this.btn_exit);		
		//>>>>>> 
		
		
		
	}
}
