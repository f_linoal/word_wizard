package menu.dialogs;

import game.dialogs.SinglePlayDialog;

import java.awt.event.ActionEvent;

import menu.dialogs.data.ComfirmWordsList;
import menu.dialogs.data.DataManager;
import menu.dialogs.data.Question;
import menu.dialogs.data.QuestionMaker;

public class KenkyuComfirmWordsDialog extends MinaraiComfirmWordsDialog {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	int wnum;
	
	public void actionPerformed(ActionEvent e){		//操作に対する動作を定義
		if(e.getSource()==this.btn_start){
			QuestionMaker.kenkyuMaker(question, wnum);
			DataManager.PointReset();
			SinglePlayDialog singleplaydialog = new SinglePlayDialog(QuestionMaker.question);
			singleplaydialog.setLocationRelativeTo(null);
			comfirmwordslist.dispose();
			this.dispose();
		}
		if(e.getSource()==this.btn_back){
			KenkyuLevelSelectDialog wordf = new KenkyuLevelSelectDialog();
			wordf.setLocationRelativeTo(null);
			wordf.setVisible(true);
			comfirmwordslist.dispose();
			this.dispose();
		}
	}

	KenkyuComfirmWordsDialog(Question[] question,int level,int wnum){
		super(question,level);
		this.question = question;
		this.wnum = wnum;
		lbl_message.setText("以下の単語から出題されます");
		comfirmwordslist = new ComfirmWordsList(question);
		p_comfirmwordslist = comfirmwordslist.getPanel();
		p_comfirmwordslist.setOpaque(false);
	}
}
