package menu.dialogs;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import menu.dialogs.data.Condition;
import menu.dialogs.data.DataManager;
import menu.dialogs.data.ScorePanel;
import menu.dialogs.gui.ImagePanel;

public class RecordDialog extends JFrame implements ActionListener {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	JButton btn_back;
	JLabel lbl_title;
	JLabel lbl_colum;
	int highscore_array[];
	JPanel scorepanel_p;
	JPanel bufpanel;

	public void actionPerformed(ActionEvent e){		//操作に対する動作を定義
		if(e.getSource()==this.btn_back){
			ModeSelectDialog modeselectf = new ModeSelectDialog();
			modeselectf.setLocationRelativeTo(null);
			modeselectf.setVisible(true);
			dispose();
		}
	}
	
	public RecordDialog(){		//ボタンなどの用意、その他の初期設定
		
		//>>>>>> フレームパラメータ等のデフォルト値を設定 >>>>>>
		this.setTitle("みならいの魔法使い");
		this.setSize(800,600);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ImagePanel imagepanel = new ImagePanel("/menu/dialogs/gui/background.jpg");
		this.add(imagepanel);
		//<<<<<<
		
		DataManager.LoadScore(1);
		this.highscore_array = DataManager.highscore_array;
		
		//>>>>>> 部品の用意 >>>>>>
		this.lbl_title = new JLabel(new ImageIcon("./dialogs_pics/highscore_logo.png"));
		
		String path="./dialogs_pics/backIcon";
		this.btn_back = new JButton(new ImageIcon(path+".gif"));
		this.btn_back.addActionListener(this);
		this.btn_back.setContentAreaFilled(false);
		this.btn_back.setBorderPainted(false);
		this.btn_back.setRolloverIcon(new ImageIcon(path+"O.gif"));
		this.btn_back.setPressedIcon(new ImageIcon(path+"P.gif"));
		//<<<<<<
		
		//>>>>>> レイアウト >>>>>>
		//>>>>>> レイアウト >>>>>>
		JPanel layer1p = new JPanel();
		layer1p.setOpaque(false);
		imagepanel.add(layer1p);
		layer1p.setLayout(new BoxLayout(layer1p,BoxLayout.Y_AXIS));
		
		JPanel layer2_0p = new JPanel();
		layer2_0p.setOpaque(false);
		layer2_0p.setLayout(new FlowLayout());
		
		JPanel layer2_1p = new JPanel();
		layer2_1p.setOpaque(false);
		layer2_1p.setLayout(new FlowLayout());
		
		JPanel layer2_2p = new JPanel();
		layer2_2p.setOpaque(false);
		layer2_2p.setLayout(new BoxLayout(layer2_2p,BoxLayout.X_AXIS));
		
		JPanel highscore_p = new JPanel();
		highscore_p.setOpaque(false);
		highscore_p.setLayout(new GridLayout(4,4));
		
		ScorePanel scorepanel;
		double bairitu=0.4,bairitu2=0.5;
		highscore_p.add(new JLabel(""));
		highscore_p.add(getRecordComponentPanel("recordLevel1Icon",bairitu2));
		highscore_p.add(getRecordComponentPanel("recordLevel2Icon",bairitu2));
		highscore_p.add(getRecordComponentPanel("recordLevel2Icon",bairitu2));
		highscore_p.add(getRecordComponentPanel("recordMinaraiIcon",bairitu2));
		int i;
		for(i=0;i<3;i++){
			scorepanel = new ScorePanel(highscore_array[i],bairitu);
			scorepanel_p = scorepanel.getNumPanel();
			scorepanel_p.setOpaque(false);
			highscore_p.add(scorepanel_p);
		}
		highscore_p.add(getRecordComponentPanel("recordKenkyuIcon",bairitu2));
		for(i=3;i<6;i++){
			scorepanel = new ScorePanel(highscore_array[i],bairitu);
			scorepanel_p = scorepanel.getNumPanel();
			scorepanel_p.setOpaque(false);
			highscore_p.add(scorepanel_p);
		}
		highscore_p.add(getRecordComponentPanel("recordBoukenIcon",bairitu2));
		for(i=6;i<9;i++){
			scorepanel = new ScorePanel(highscore_array[i],bairitu);
			scorepanel_p = scorepanel.getNumPanel();
			scorepanel_p.setOpaque(false);
			highscore_p.add(scorepanel_p);
		}

		JPanel highscore_outer_p = new JPanel();
		highscore_outer_p.setOpaque(false);
		highscore_p.setPreferredSize(new Dimension(800,300));
		highscore_outer_p.add(highscore_p);


		layer1p.add(layer2_0p);
		//layer1p.add(Box.createRigidArea(new Dimension(300,0)));
		layer1p.add(layer2_1p);
		layer1p.add(Box.createRigidArea(new Dimension(50,10)));
		layer1p.add(layer2_2p);
		
		layer2_0p.add(lbl_title);
		layer2_1p.add(highscore_p);
		layer2_2p.add(btn_back);
		//>>>>>> 
		
	}
	
	public JPanel getRecordComponentPanel(String path,double bairitu2){
		Image image;ImageIcon imageicon;
		imageicon = new ImageIcon("./dialogs_pics/"+path+".png");
		image = imageicon.getImage().getScaledInstance((int) (imageicon.getIconWidth() * bairitu2), -1,Image.SCALE_SMOOTH);
		imageicon = new ImageIcon(image);
		JLabel buf_lbl = new JLabel(imageicon);
		bufpanel = new JPanel();
		bufpanel.setLayout(new FlowLayout());
		bufpanel.setOpaque(false);
		bufpanel.add(buf_lbl);
		return bufpanel;
	}
	
	public static void main(String[] args) {
		ModeSelectDialog modeselect = new ModeSelectDialog();
		modeselect.setLocationRelativeTo(null);
		modeselect.setVisible(true);
		Condition.count_num();
	}
}