package game.dialogs;

import menu.dialogs.data.Question;



public class SinglePlayDialog extends GameDialog {
	private static final long serialVersionUID = 8596496361771638271L;


	public SinglePlayDialog(Question[] questionList){
		super(new SinglePlayPanel(), questionList);
		System.out.println("Starting Single Play Dialog");
	}
	
	public static void main(String[] args) {
		new SinglePlayDialog(null); // TODO
	}

}
