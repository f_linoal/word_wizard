package game.dialogs;

import menu.dialogs.data.Question;


public class MultiPlayDialog extends GameDialog {
	private static final long serialVersionUID = -4918866274985788295L;

	public MultiPlayDialog(Question[] questionList){
		super(new MultiPlayPanel(), questionList);
		System.out.println("Starting Multi Play Dialog");
		// ここで通信初期化
	}
	
	public static void main(String[] args) {
		new MultiPlayDialog(null);  // TODO
	}
}
