package game.dialogs;
// シングルプレイ、マルチプレイ画面の基底クラス


import game.logic.managers.GameDataManager;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JDialog;
import javax.swing.WindowConstants;

import menu.dialogs.data.Question;

public class GameDialog extends JDialog implements WindowListener {
	private static final long serialVersionUID = 3460289437149103246L;
	private GamePanel gamePanel;

	// 引数：どの GamePanel を使うか？　例： new SinglePlayPanel() または new MultiPlayPanel()
	GameDialog(GamePanel _gamePanel, Question[] questionList){
		super();
		System.out.println("Starting GameDialog");
		initialize(_gamePanel, questionList);
	}

	public GameDialog(JDialog owner, GamePanel _gamePanel, Question[] questionList) {
		super(owner);
		initialize(_gamePanel, questionList);
	}
	
	private void initialize(GamePanel _gamePanel, Question[] questionList){
		GameDataManager.setQuestionList(questionList);
		GameDataManager.setGameDialog(this);
		Dimension dialogSize = GameDataManager.defaultWindowSize;
		gamePanel = _gamePanel;
		gamePanel.setSize(dialogSize);
		setSize(dialogSize);
		add(gamePanel, BorderLayout.CENTER);
		setTitle("Apprentice Wizard");
		setVisible(true);
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		addWindowListener(this);
	}

	@Override
	public void windowOpened(WindowEvent e) {
	}

	@Override
	public void windowClosing(WindowEvent e) {
	}

	@Override
	public void windowClosed(WindowEvent e) {
		gamePanel.closeThread();
		System.out.println("Closing GameDialog.");
	}

	@Override
	public void windowIconified(WindowEvent e) {
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
	}

	@Override
	public void windowActivated(WindowEvent e) {
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
	}
	
	public GamePanel getGamePanel(){
		return gamePanel;
	}
}
