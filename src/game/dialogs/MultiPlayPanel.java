package game.dialogs;

import game.logic.managers.GameDataManager;
import game.logic.managers.GameInitializerMulti;

import java.awt.Color;
import java.awt.Graphics;

public class MultiPlayPanel extends GamePanel {
	private static final long serialVersionUID = -6899030206484782559L;

	MultiPlayPanel(){
		super();
		System.out.println("Starting Multi Play Panel");
		GameDataManager.init(new GameInitializerMulti(), this);
		super.startThread();
	}
	
	@Override
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		g.setColor(Color.RED);
	}

	@Override
	public void attackEnemyWithAns(String ans) {
		GameDataManager.getPlayer().turnNearestApproachingEnemy(ans);
	}
}
