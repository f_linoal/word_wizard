package game.dialogs;
import game.logic.managers.GameDataManager;
import game.logic.managers.GameInitializerSingle;

import java.awt.Color;
import java.awt.Graphics;


public class SinglePlayPanel extends GamePanel {
	private static final long serialVersionUID = 7759200218740607665L;
	
	SinglePlayPanel(){
		super();
		System.out.println("Starting Single Play Panel");
		GameDataManager.init(new GameInitializerSingle(), this);
		super.startThread();
	}
	
	@Override
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		g.setColor(Color.RED);
	}

	@Override
	public void attackEnemyWithAns(String ans) {
		GameDataManager.getPlayer().attackNearestEnemy(ans);
	}
}
