package game.dialogs;
// ゲーム描画用のパネルの基底クラス。このサブクラスがGameDialogの中に配置される。

import game.logic.gameobjects.PlayerBalloon;
import game.logic.managers.GameDataManager;
import game.logic.managers.GameObjectManager;
import game.logic.managers.TagManager;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JPanel;

// 初期化後 startThread を呼び出してゲーム開始
public abstract class GamePanel extends JPanel implements Runnable, KeyListener {
	private static final long serialVersionUID = 4798445055965105091L;
	private Thread gameThread;
	private boolean isMainLoopActive = true; 
	private GameObjectManager gameObjectManager;
	
	GamePanel(){
		System.out.println("Starting GamePanel");
		gameThread = new Thread(this);
		setSize(GameDataManager.defaultWindowSize);
		setFocusable(true);
		addKeyListener(this);
	}
	
	@Override
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		gameObjectManager.drawAll((Graphics2D)g);
		g.setColor(Color.BLUE);
	}

	@Override
	public void run() {
		gameObjectManager = GameDataManager.getObjectManager();
		while(isMainLoopActive){
			try{
				gameObjectManager.calcAll();
				repaint();
				revalidate();
				Thread.sleep(1000/60);
			}catch(InterruptedException e){
				e.printStackTrace();
			}
		}
		System.out.println("Closing gameThread");
	}
	
	public void startThread(){
		gameThread.start();
	}
	public void closeThread(){
		isMainLoopActive = false;
	}
	
	public abstract void attackEnemyWithAns(String ans);

	@Override
	public void keyTyped(KeyEvent e) {
	}

	// オブジェクトにキー情報を送出
	@Override
	public void keyPressed(KeyEvent e) {
		PlayerBalloon playerBalloon = (PlayerBalloon) GameDataManager.getObjectManager().findByTag(TagManager.PLAYER_BALLOON);
		int keyChar = e.getKeyChar();
		int keyCode = e.getKeyCode();
		switch(keyCode){
		case KeyEvent.VK_BACK_SPACE:
			playerBalloon.backSpace();
			break;
		case KeyEvent.VK_ENTER:
			attackEnemyWithAns(playerBalloon.getText());
			playerBalloon.resetText();
			break;
		default:
			if( keyChar != KeyEvent.CHAR_UNDEFINED ){
				playerBalloon.appendChar((char)keyChar);
			}
			break;
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
	}
}
