package game.logic.managers;

public class Coordinates {
	public float x;
	public float y;
	
	public Coordinates(float _x, float _y) {
		x = _x;
		y = _y;
	}
	
	// target 方向に speed 距離分だけ移動
	public void moveTo(Coordinates target, float speed){
		Coordinates diff = target.minus(this);
		float absoluteValue = diff.absoluteValue();
		if(absoluteValue == 0){
			return;  // 距離0なら動かない
		}
		Coordinates dr_dt = diff.div(absoluteValue).times(speed);
		this.set(this.plus(dr_dt));
	}
	
	public Coordinates minus(Coordinates coord){
		return new Coordinates(x - coord.x, y - coord.y);
	}
	
	public Coordinates plus(Coordinates coord){
		return new Coordinates(x + coord.x, y + coord.y);
	}
	
	public Coordinates times(float num){
		return new Coordinates(x*num, y*num);
	}
	
	public Coordinates div(float num){
		try{
			return new Coordinates(x/num, y/num);
		}catch(ArithmeticException e){
			e.printStackTrace();
			System.out.println("Error: Coordinates is devided by zero.");
			return null;
		}
	}
	
	public float absoluteValue(){
		return (float)Math.sqrt(x*x + y*y);
	}
	
	public float distanceFrom(Coordinates coord){
		return this.minus(coord).absoluteValue();
	}
	
	public void set(Coordinates coord){
		x = coord.x;
		y = coord.y;
	}
	
	public Coordinates copy(){
		return new Coordinates(x,y);
	}

}
