package game.logic.managers;
// レイヤー名と番号の対応付けをするクラス。
public class Layer {
	public static final int EFFECT = 1;
	public static final int HUD = 2;
	public static final int ENEMY = 3;
	public static final int PLAYER = 4;
	public static final int BACKGROUND_OBJECT = 5;
	public static final int BACKGROUND_IMAGE = 6;
	public static final int INFINITE_DEPTH = Integer.MAX_VALUE;
}
