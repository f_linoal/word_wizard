package game.logic.managers;

import java.awt.Image;
import java.awt.image.ImageObserver;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;

public class ImageManager implements ImageObserver {
	
	// 画像の呼び出しキー一覧 ===========
	public static final String PLAYER = "player";
	public static final String OPPOSITE_PLAYER = "oppositePlayer";
	public static final String ENEMY = "enemy";
	public static final String BACK_GROUND = "backGround";
	public static final String BALLOON = "balloon";
	
	// 画像ファイルのディレクトリ
	private final String IMAGE_RELATIVE_DIR = "game_graphics/";
	
	// 画像呼び出しのハッシュ
	private HashMap<String, String> keyToImageNameHash = new HashMap<String, String>();
	private HashMap<String, Image> nameToImageHash = new HashMap<String, Image>();
	
	private void organizeKeyToImageNameHash(){
		// 画像ファイルの名前 ===========
		HashMap<String, String> hash = keyToImageNameHash;
		hash.put(PLAYER, "wizard1.png");
		hash.put(OPPOSITE_PLAYER, "wizard2.png");
		hash.put(ENEMY, "enemy1.png");
		hash.put(BACK_GROUND, "field1.png");
		hash.put(BALLOON, "balloon2.png");
	}
	
	public ImageManager(){
		URL imageURL = null;
		try {
			// キーと画像ディレクトリの対応付け
			URL imageUrl = new URL("file:" + IMAGE_RELATIVE_DIR);
			organizeKeyToImageNameHash();
			// キーと画像オブジェクトの対応付け
			for( Map.Entry<String,String> cih : keyToImageNameHash.entrySet() ){
				imageURL = new URL(imageUrl, cih.getValue());
				Image image = ImageIO.read(imageURL);
				nameToImageHash.put(cih.getKey(), image);
			}
			
		} catch (MalformedURLException e) {
			System.err.println("Folder for game graphics was not found.");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("Image " + imageURL + " was not found.");
		}
	}
	
	public Image get(String imageKey){
		return nameToImageHash.get(imageKey);
	}

	@Override
	public boolean imageUpdate(Image image, int flag, int arg2, int arg3,
			int arg4, int arg5) {
		return false;
	}
}
