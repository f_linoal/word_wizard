package game.logic.managers;
// 全てのゲームオブジェクトを管理するクラス
import game.logic.gameobjects.GameObject;
import game.logic.gameobjects.GameObjectLayerComparator;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.Collections;


public class GameObjectManager {
	ArrayList<GameObject> gameObjects;
	ArrayList<GameObject> addList = new ArrayList<GameObject>();
	ArrayList<GameObject> removeList = new ArrayList<GameObject>();
	
	GameObjectManager(){
		gameObjects = new ArrayList<GameObject>();
	}
	
	public void drawAll(Graphics2D g){
		for( GameObject o : gameObjects ){
			o.drawChilds(g);
		}
	}
	
	public void calcAll(){
		// リストに追加
		for( GameObject o : addList ){
			o.init();
			gameObjects.add(o);
		}
		// レイヤー順にソート
		if( addList != null && addList.size() > 0){
			Collections.sort(gameObjects, new GameObjectLayerComparator());
		}
		addList.clear();
		
		// リスト内で計算
		for( GameObject o : gameObjects ){
			o.calcChilds();
		}
		
		// リストから削除
		for( GameObject obj : removeList ){
			gameObjects.remove(obj);
		}
		removeList.clear();
	}
	
	public void add(GameObject obj){
		addList.add(obj);
	}
	
	public void remove(GameObject obj){
		removeList.add(obj);
	}
	
	public void initAll(){
		for( GameObject o : getObjlistAndAddlist()){
			o.init();
		}
	}
	
	// 指定タグのオブジェクトを1つだけ検索
	public GameObject findByTag(String _tag){
		for( GameObject o : getObjlistAndAddlist()){
			if( o.getTag().equals(_tag) ){
				return o;
			}
		}
		return null;  // 見つからない場合
	}
	
	public ArrayList<GameObject> findAllByTag(String _tag){
		ArrayList<GameObject> matches = new ArrayList<GameObject>();
		for( GameObject o : getObjlistAndAddlist() ){
			if( o.getTag() == null){
				System.err.println("Error: " + o.getClass().getName() + " has no tag!");
			}
			if( o.getTag().equals(_tag)){
				matches.add(o);
			}
		}
		return matches;
	}
	
	// オブジェクトリストと addList を1つにして返す
	private ArrayList<GameObject> getObjlistAndAddlist(){
		ArrayList<GameObject> dst = new ArrayList<GameObject>();
		dst.addAll(gameObjects);
		dst.addAll(addList);
		return dst;
	}
	
	
}
