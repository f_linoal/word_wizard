package game.logic.managers;

import game.dialogs.GameDialog;
import game.logic.gameobjects.GameObject;
import game.logic.gameobjects.Wizard;

import java.awt.Dimension;
import java.util.ArrayList;

import javax.swing.JPanel;

import menu.dialogs.ResultDialog;
import menu.dialogs.data.Question;


public class GameDataManager {
	public static final Dimension defaultWindowSize = new Dimension(640, 480);
	public static GameObjectManager gameObjectManager;
	private static ImageManager imageManager;
	private static GameDialog gameDialog;
	private static JPanel gamePanel;
	private static LayoutManager layoutManager;
	private static Question[] questionList;
	private static int questionIndex;
	
	public static void resetVars(){
		gameObjectManager = null;
		imageManager = null;
		gameDialog = null;
		gamePanel = null;
		layoutManager = null;
		questionList = null;
		questionIndex = 0;
	}
	
	public static void init(GameInitializer initializer, JPanel _gamePanel){
		System.out.println("Initializing Game Data Manager");
		gamePanel = _gamePanel;
		gameObjectManager = new GameObjectManager();
		imageManager = new ImageManager();
		layoutManager = new LayoutManager(gamePanel);
		initializer.init();
		gameObjectManager.initAll();  // Entityを全て作った後に initAll する必要がある。
	}
	
	public static void setQuestionList(Question[] _questionList){
		questionIndex = 0;
		questionList = _questionList;
	}
	
	// 終了時に閉じるべきダイアログを指定（必須）
	public static void setGameDialog(GameDialog _gameDialog){
		gameDialog = _gameDialog;
	}
	
	public static GameObjectManager getObjectManager(){
		return gameObjectManager;
	}
	
	public static LayoutManager getLayoutManager(){
		return layoutManager;
	}
	
	public static Question getNextQuestion(){
		if( questionIndex >= questionList.length ){
			endGame();
			return null;
		}
		return questionList[questionIndex++];
	}
	
	public static Question getQuestionByIndex(long index){
		return questionList[(int) index];
	}
	
	public static void endGame(){
		gameDialog.getGamePanel().closeThread();
		new ResultDialog();
		gameDialog.dispose();
		resetVars();
	}
	
	public static ImageManager getImageManager(){
		return imageManager;
	}
	
	public static JPanel getGamePanel(){
		return gamePanel;
	}
	
	// うっかり同じものを作ってしまった。（Wizardクラスにある）
	public static Wizard getPlayer(){
		ArrayList<GameObject> wizards = gameObjectManager.findAllByTag(TagManager.PLAYER);
		for( GameObject w : wizards ){
			if( ((Wizard)w).isPlayer() ){
				return (Wizard)w;
			}
		}
		return null;
	}
}
