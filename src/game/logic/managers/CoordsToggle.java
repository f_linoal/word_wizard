package game.logic.managers;


// 1回ごとに diff だけ座標を変え、 mod 回で元に戻る座標トグルマネジャ。 
public class CoordsToggle {
	private Coordinates base;
	private int mod;
	private Coordinates diff;
	private int state;
	
	public CoordsToggle(Coordinates _base, int _mod, Coordinates _diff){
		base = _base;
		mod = _mod;
		diff = _diff;
		state = 0;
	}
	
	public Coordinates toggle(){
		state = ++state % mod;
		return base.plus(diff.times(state));
	}
	
}
