package game.logic.managers;

import javax.swing.JPanel;

public class LayoutManager {
	private JPanel panel;
	private float groundY = 0.6f;  // 単位 %  地面の位置
	private float playerX = 0.15f;
	private float oppositePlayerX = 0.9f;
	private float inputBalloonY = 0.8f;
	private int inputBalloonXOffset = 20;  // 入力窓が画面左端からどれだけ右にずれているか。 単位 px
	private float enemySpawnSingleMarginX = 30;  // シングルのスポーン位置が画面右端からどれだけ外側に離れているか。 単位 px
	public int enemySpawnMultiMarginY = -100;

	public LayoutManager(JPanel _panel) {
		panel = _panel;
	}
	
	public int getGroundY(){
		return (int) (panel.getSize().height * groundY);
	}
	
	public Coordinates getPlayerCoord(){
		return new Coordinates( (int)(panel.getSize().width * playerX), getGroundY());
	}
	
	public Coordinates getOppositePlayerCoord(){
		return new Coordinates( (int)(panel.getSize().width * oppositePlayerX), getGroundY());
	}
	
	public Coordinates getEnemySpawnerSingleCoord(){
		return new Coordinates( panel.getSize().width + enemySpawnSingleMarginX, getGroundY() );
	}
	
	public Coordinates getEnemySpawnerMultiCoord(){
		return new Coordinates(panel.getSize().width / 2, getGroundY());
	}
	
	public Coordinates getInputBalloonCoord(){
		return new Coordinates(inputBalloonXOffset, panel.getSize().height * inputBalloonY);
	}

}
