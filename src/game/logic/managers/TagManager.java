package game.logic.managers;

public class TagManager {
	public static final String ENEMY = "Enemy";
	public static final String PLAYER = "Player";
	public static final String HUD = "HUD";
	public static final String PLAYER_BALLOON = "PlayerBalloon";
	public static final String ENEMY_SPAWNER = "EnemySpawner";
	public static final String BACK_GROUND = "BackGround";
}
