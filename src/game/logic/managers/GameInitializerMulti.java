package game.logic.managers;

import game.logic.gameobjects.BackGround;
import game.logic.gameobjects.EnemySpawnerMulti;
import game.logic.gameobjects.HUD;
import game.logic.gameobjects.PlayerBalloon;
import game.logic.gameobjects.Wizard;

public class GameInitializerMulti extends GameInitializer {

	@Override
	public void init() {
		System.out.println("Initializing @ Game Initializer Multi");
		LayoutManager layout = GameDataManager.getLayoutManager();
		GameObjectManager objManager = GameDataManager.getObjectManager();
		// 初期エンティティの配置
		objManager.add(new EnemySpawnerMulti());
		objManager.add(new Wizard(Layer.PLAYER, layout.getPlayerCoord(), true));
		objManager.add(new Wizard(Layer.PLAYER, layout.getOppositePlayerCoord(), false));
		objManager.add(new HUD());
		objManager.add(new PlayerBalloon());
		objManager.add(new BackGround());
	}

}
