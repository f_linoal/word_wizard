package game.logic.managers;
// ゲーム初期化処理の基底クラス
public abstract class GameInitializer {
	public abstract void init();
}
