package game.logic.managers;

import game.logic.gameobjects.BackGround;
import game.logic.gameobjects.EnemySpawner;
import game.logic.gameobjects.HUD;
import game.logic.gameobjects.PlayerBalloon;
import game.logic.gameobjects.Wizard;


public class GameInitializerSingle extends GameInitializer {

	@Override
	public void init() {
		System.out.println("Initializing @ Game Initializer Single");
		GameObjectManager objManager = GameDataManager.getObjectManager();
		LayoutManager layout = GameDataManager.getLayoutManager();
		objManager.add(new EnemySpawner(layout.getEnemySpawnerSingleCoord()));  // 初期エンティティの配置
		objManager.add(new Wizard(Layer.PLAYER, layout.getPlayerCoord(), true));
		objManager.add(new HUD());
		objManager.add(new PlayerBalloon());
		objManager.add(new BackGround());
	}

}
