package game.logic.gameobjects;

import game.logic.managers.Coordinates;
import game.logic.managers.GameDataManager;
import game.logic.managers.ImageManager;
import game.logic.managers.Layer;
import game.logic.managers.TagManager;

import java.awt.Color;
import java.awt.Graphics2D;

public class Balloon extends Entity {
	private static final int LAYER = Layer.HUD;
	private static final String TAG = TagManager.HUD;
	private int sizeX = 150;
	private int sizeY = 20;
	protected int textMarginBottom = 5;
	protected int textMarginLeft = 5;
	protected String text = "";

	public Balloon(Coordinates _coord) {
		super(LAYER, TAG, _coord, 1, GameDataManager.getImageManager().get(ImageManager.BALLOON));
	}
	
	// 継承用のコンストラクタ
	public Balloon(int layer, String tag, Coordinates coordinates){
		super(layer, tag, coordinates, 1, null);
	}

	@Override
	public void draw(Graphics2D g) {
		Coordinates coord = getAbsoluteCoord();
		g.setColor(Color.WHITE);
		g.fillRect((int)coord.x, (int)coord.y, sizeX, sizeY);
		g.setColor(Color.BLACK);
		g.drawString(text, coord.x + textMarginLeft, coord.y + sizeY - textMarginBottom);
	}
	
	public void setText(String _text){
		text = _text;
	}

	@Override
	public void calc() {
		// TODO Auto-generated method stub

	}

	@Override
	public void init() {
		// TODO Auto-generated method stub

	}

}
