package game.logic.gameobjects;

import java.util.Comparator;

public class GameObjectLayerComparator implements Comparator<GameObject>{
	@Override
	public int compare(GameObject o1, GameObject o2){
		return o2.layer - o1.layer;
	}
}
