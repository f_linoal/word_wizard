package game.logic.gameobjects;

import game.logic.managers.Coordinates;
import game.logic.managers.GameDataManager;
import game.logic.managers.ImageManager;
import game.logic.managers.TagManager;
import game.telecommunication.Client;

import java.awt.Image;
import java.util.ArrayList;

import menu.dialogs.data.DataManager;


public class Wizard extends Entity {
	private static final String TAG = TagManager.PLAYER;
	private static int MAX_HIT_POINT = 10;
	private boolean isPlayer;

	public Wizard(int _layer, Coordinates _coordinates, boolean _isPlayer) {
		super(_layer, TAG, _coordinates, MAX_HIT_POINT, chooseImage(_isPlayer));
		isPlayer = _isPlayer;
	}

	@Override
	public void calc() {
		// TODO Auto-generated method stub

	}

	@Override
	public void init() {
		// TODO Auto-generated method stub
		
	}
	
	public static Image chooseImage(boolean isPlayer){
		ImageManager imgManager = GameDataManager.getImageManager();
		if( isPlayer ){
			return imgManager.get(ImageManager.PLAYER);
		}
		else{
			return imgManager.get(ImageManager.OPPOSITE_PLAYER);
		}
	}
	
	// マルチプレイの攻撃
	public void turnNearestApproachingEnemy(String ans){
		Enemy nearestEnm = Enemy.searchNearestApproaching(this);
		if( nearestEnm != null ){
			nearestEnm.receiveTurnMagic(ans);
			// ここに通信処理
			Client.sendMessage("turnEnemy " + nearestEnm.getId());
		}
	}
	
	// シングルプレイの攻撃
	public void attackNearestEnemy(String ans){
		Enemy nearestEnm = (Enemy)searchNearestInTag(TagManager.ENEMY);
		if( nearestEnm != null ){
			nearestEnm.receiveAttackMagic(ans);
		}
	}
	
	@Override
	public void fallDown(){
		super.fallDown();
		if(isPlayer){
			DataManager.win_flag = 0;
		}else{
			DataManager.win_flag = 1;
		}
		GameDataManager.endGame();
	}
	
	public boolean isPlayer(){
		return isPlayer;
	}
	
	// 魔法使いのうちプレイヤーである者を1人返す。
	public static Wizard getPlayer(){
		return getByPlayerFlag(true);
	}
	
	// プレイヤーでない魔法使いのうち1人を返す。
	public static Wizard getOpposite(){
		return getByPlayerFlag(false);
	}
	
	// 上で使用
	public static Wizard getByPlayerFlag(boolean playerFlag){
		ArrayList<GameObject> wizards = GameDataManager.getObjectManager().findAllByTag(TagManager.PLAYER);
		for( GameObject w_obj : wizards){
			Wizard w_wiz = (Wizard)w_obj;
			if( w_wiz.isPlayer == playerFlag ){
				return w_wiz;
			}
		}
		return null;
	}

}
