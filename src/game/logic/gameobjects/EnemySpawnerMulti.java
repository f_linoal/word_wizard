package game.logic.gameobjects;

import java.util.ArrayList;

import game.logic.managers.Coordinates;
import game.logic.managers.GameDataManager;
import game.logic.managers.TagManager;

public class EnemySpawnerMulti extends EnemySpawner {

	public EnemySpawnerMulti() {
		super(new Coordinates(-1,-1));  // 仮
	}
	
	@Override
	public void calc(){
		// 何もしない
	}
	
	@Override
	public void init(){
		ArrayList<GameObject> wizards = GameDataManager.getObjectManager().findAllByTag(TagManager.PLAYER);
		// 2人の中間にスポナー配置
		coordinates = wizards.get(0).coordinates.plus(wizards.get(1).coordinates).div(2).plus(new Coordinates(0, GameDataManager.getLayoutManager().enemySpawnMultiMarginY));
	}
	
	// 引数： trueなら自分向き、 falseなら相手向き。
	// 注： 魔法使いは2人以内を想定。
	public void spawnEnemyWithDirection(boolean isDirectionTowardMe, int id){
		Enemy enemy = new Enemy(coordinates.copy(), id);
		if(isDirectionTowardMe){
			enemy.setTarget(Wizard.getPlayer());
		}else{
			enemy.setTarget(Wizard.getOpposite());
		}
		GameDataManager.getObjectManager().add(enemy);
	}

}
