package game.logic.gameobjects;


import game.logic.managers.GameDataManager;
import game.logic.managers.Layer;
import game.logic.managers.TagManager;

public class PlayerBalloon extends Balloon {
	private static final int LAYER = Layer.HUD;
	private static final String TAG = TagManager.PLAYER_BALLOON;
	
	public PlayerBalloon() {
		super(LAYER, TAG, GameDataManager.getLayoutManager().getInputBalloonCoord());
	}
	
	public void appendChar(char c){
		text += c;
	}
	
	public void backSpace(){
		if( text.length() == 0 ){
			return;
		}
		text = text.substring(0, text.length()-1);
	}

	public void resetText() {
		text = "";
	}

	public String getText() {
		return text;
	}

}
