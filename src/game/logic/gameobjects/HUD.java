package game.logic.gameobjects;

import game.logic.managers.GameDataManager;
import game.logic.managers.Layer;
import game.logic.managers.TagManager;

import java.awt.Color;
import java.awt.Graphics2D;

import menu.dialogs.data.DataManager;


public class HUD extends GameObject {
	private static final int LAYER = Layer.HUD;
	private static final String TAG = TagManager.HUD;
	private Wizard player;
	private int playerHP = -1;
	private int highScore = -1;
	private int score = 0;

	public HUD() {
		super(LAYER, TAG, null);
	}

	@Override
	public void init() {
		player = GameDataManager.getPlayer();
		highScore = DataManager.highscore;
		if( player == null ){
			System.out.println("player not found");
		}
	}

	@Override
	public void draw(Graphics2D g) {
		calcTextOnTopLeft();
		g.setColor(Color.RED);
		g.drawString(calcTextOnTopLeft(), 20, 20);
	}

	@Override
	public void calc() {
		playerHP = player.getHP();
		score = DataManager.score;
	}
	
	private String calcTextOnTopLeft(){
		String ret = "";
		ret += "HP: " + playerHP + " \n";
		ret += "HighScore: " + highScore + " \n";
		ret += "Score: " + score + " \n";
		return ret;
	}

}
