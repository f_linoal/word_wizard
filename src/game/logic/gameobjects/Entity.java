package game.logic.gameobjects;

import game.logic.managers.Coordinates;
import game.logic.managers.GameDataManager;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.ImageObserver;


public abstract class Entity extends GameObject {
	protected int hitPoint;
	protected Image image;

	@Override public abstract void calc();
	
	Entity(int _layer, String _tag, Coordinates _coordinates, int _hitPoint, Image _image) {
		super(_layer, _tag, _coordinates);
		hitPoint = _hitPoint;
		image = _image;
	}
	
	@Override
	public void draw(Graphics2D g){
		if( image != null ){
			ImageObserver iobs = GameDataManager.getImageManager();
			Coordinates imageCoord = calcImageCoord(image, iobs);
			g.drawImage(image, (int)imageCoord.x, (int)imageCoord.y, iobs);
		}
	}
	
	// ���]�\��
	public void drawFlippedHorizontal(Graphics2D g){
		ImageObserver iobs = GameDataManager.getImageManager();
		Coordinates imageCoord = calcImageCoord(image, iobs);
		Coordinates coordTopRight = imageCoord.plus(new Coordinates(image.getWidth(iobs), 0));
		drawScaledImage(g, (int)coordTopRight.x, (int)coordTopRight.y, -image.getWidth(iobs), image.getHeight(iobs));// ���E�t�ɂ��Ă���
	}
	
	public Coordinates calcImageCoord(Image image, ImageObserver iobs){
		Coordinates imageSize = new Coordinates(image.getWidth(iobs), image.getHeight(iobs));
		return coordinates.minus(imageSize.div(2));
	}

	public void drawScaledImage(Graphics2D g, int x, int y, int dx, int dy){
		if( image != null ){
			ImageObserver iobs = GameDataManager.getImageManager();
			g.drawImage(image, x, y, dx, dy, iobs);
		}
	}
	
	public void attackTo(Entity target, int damage){
		target.receiveDamage(damage);
	}

	public void fallDown(){
		GameDataManager.getObjectManager().remove(this);
	}
	
	public void receiveDamage(int damage){
		hitPoint -= damage;
		if( this.hitPoint <= 0 ){
			fallDown();
		}
	}
	
	public int getHP(){
		return hitPoint;
	}
	
	public boolean isDead(){
		return hitPoint <= 0;
	}
}
