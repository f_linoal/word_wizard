package game.logic.gameobjects;

import game.logic.managers.Coordinates;
import game.logic.managers.GameDataManager;
import game.logic.managers.ImageManager;
import game.logic.managers.Layer;
import game.logic.managers.TagManager;

import java.awt.Graphics2D;
import java.awt.image.ImageObserver;

import javax.swing.JPanel;

public class BackGround extends Entity {
	private static final int LAYER = Layer.BACKGROUND_IMAGE;
	private static final String TAG = TagManager.BACK_GROUND;

	public BackGround() {
		super(LAYER, TAG, new Coordinates(-1,-1), 1, GameDataManager.getImageManager().get(ImageManager.BACK_GROUND));
	}
	
	@Override
	public void draw(Graphics2D g){
		ImageObserver observer = GameDataManager.getImageManager();
		JPanel gamePanel = GameDataManager.getGamePanel();
		g.drawImage(image, 0, 0, gamePanel.getWidth() , gamePanel.getHeight(), observer);
	}

	@Override
	public void calc() {
		// 何もしない
	}

	@Override
	public void init() {
		// 何もしない
	}

}
