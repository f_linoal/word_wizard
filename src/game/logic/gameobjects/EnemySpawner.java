package game.logic.gameobjects;

import game.logic.managers.Coordinates;
import game.logic.managers.CoordsToggle;
import game.logic.managers.GameDataManager;
import game.logic.managers.Layer;
import game.logic.managers.TagManager;

import java.awt.Graphics2D;

// 敵を出現させるEntity
public class EnemySpawner extends Entity {
	private static final int layer = Layer.INFINITE_DEPTH;
	private static final String tag = TagManager.ENEMY_SPAWNER;
	private int spawnInterbal = 300;  // 時間単位はフレーム
	private int spawnCountDown = 0;
	private static final int SPAWN_COORDS_MOD = 3;  // スポーン座標のパターン数
	private static final Coordinates SPAWN_COORD_DIFF = new Coordinates(0,20);  // 前のパターンとの座標差
	private CoordsToggle coordsToggle;

	// 引数：座標は敵の出現場所になる
	public EnemySpawner(Coordinates _coordinates) {
		super(layer, tag, _coordinates, 1, null);
		coordsToggle = new CoordsToggle(coordinates, SPAWN_COORDS_MOD, SPAWN_COORD_DIFF);
	}

	@Override
	public void draw(Graphics2D g) {
		// 描画しない
	}

	@Override
	public void calc() {
		if(spawnCountDown-- <= 0){
			spawnCountDown = spawnInterbal;
			spawnEnemy();
		}
	}

	@Override
	public void init() {
		// TODO Auto-generated method stub

	}
	
	public void spawnEnemy(){
		Coordinates spawnCoord = coordsToggle.toggle();
		GameDataManager.getObjectManager().add(new Enemy(spawnCoord));
	}

}
