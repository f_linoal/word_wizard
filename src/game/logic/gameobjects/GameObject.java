package game.logic.gameobjects;
// ゲームに登場する全オブジェクトの抽象基底クラス。

import game.logic.managers.Coordinates;
import game.logic.managers.GameDataManager;

import java.awt.Graphics2D;
import java.util.ArrayList;

public abstract class GameObject{
	protected String tag;
	protected Coordinates coordinates;
	protected int layer;  // 手前から何番目のレイヤーか。レイヤー＝描画順
	protected ArrayList<GameObject> childs = new ArrayList<GameObject>();
	protected GameObject parent;
	
	public abstract void init();
	public abstract void draw(Graphics2D g);
	public abstract void calc();
	
	// 生成するだけで自動的に objectManager に登録
	GameObject(int _layer, String _tag, Coordinates _coordinates){
		layer = _layer;
		coordinates = _coordinates;
		tag = _tag;
	}
	
	public void addChild(GameObject obj){
		childs.add(obj);
		obj.parent = this;
	}
	
	// 「自分と」その子オブジェクトを全て描画。再帰処理。
	public void drawChilds(Graphics2D g){
		draw(g);
		if( childs == null ){
			return;
		}
		for( GameObject obj : childs ){
			obj.drawChilds(g);
		}
	}
	
	// 「自分と」その子オブジェクトを全て計算。再帰処理。
	public void calcChilds(){
		calc();
		if( childs == null ){
			return;
		}
		for( GameObject obj : childs){
			obj.calcChilds();
		}
	}
	
	public String getTag(){
		return tag;
	}
	
	public Coordinates getCoordinates(){
		return coordinates;
	}
	
	public GameObject searchNearestInTag(String _tag){
		ArrayList<GameObject> gameObjects = GameDataManager.getObjectManager().findAllByTag(_tag);
		return searchNearestInArray(gameObjects);
	}
	
	public GameObject searchNearestInArray(Iterable<GameObject> gameObjects){
		float nearestDist = Float.MAX_VALUE;
		GameObject nearestObj = null;
		for( GameObject o : gameObjects){
			float oDist = o.coordinates.distanceFrom(this.coordinates);
			if(oDist < nearestDist){
				nearestDist = oDist;
				nearestObj = o;
			}
		}
		return nearestObj;
	}
	
	public ArrayList<GameObject> getChilds(){
		return childs;
	}
	
	// 現在の座標を親に対する相対座標とみなし、絶対座標を計算する。
	public Coordinates getAbsoluteCoord(){
		if( parent == null){
			return coordinates;
		}
		Coordinates parentCoord = parent.getCoordinates();
		return new Coordinates(coordinates.x + parentCoord.x, coordinates.y + parentCoord.y);
	}
}
