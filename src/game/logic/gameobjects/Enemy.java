package game.logic.gameobjects;

import game.logic.managers.Coordinates;
import game.logic.managers.CoordsToggle;
import game.logic.managers.GameDataManager;
import game.logic.managers.ImageManager;
import game.logic.managers.Layer;
import game.logic.managers.TagManager;
import game.telecommunication.Client;

import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.Random;

import menu.dialogs.data.Condition;
import menu.dialogs.data.DataManager;
import menu.dialogs.data.Question;


public class Enemy extends Entity {
	private static final String TAG = TagManager.ENEMY;
	private static final float ATTACK_RANGE = 5;
	private static final Coordinates BALLOON_COORD = new Coordinates(15, -50);  // 吹き出しの相対座標
	private static final int BALLOON_COORD_MOD = 3;
	private static final Coordinates BALLOON_COORD_DIFF = new Coordinates(0, 15);
	private static CoordsToggle balloonCoordsToggle = new CoordsToggle(BALLOON_COORD, BALLOON_COORD_MOD, BALLOON_COORD_DIFF);
	private static long nextId = 0;
	private long id;
	private Question question;
	private float speed = 0.4f;
	private Entity target;
	private Balloon balloon;
	
	public Enemy(Coordinates _coordinates) {
		super(Layer.ENEMY, TAG, _coordinates, 1, GameDataManager.getImageManager().get(ImageManager.ENEMY));
		id = nextId++;
	}
	
	public Enemy(Coordinates _coordinates, int _id){
		super(Layer.ENEMY, TAG, _coordinates, 1, GameDataManager.getImageManager().get(ImageManager.ENEMY));
		id = _id;
		nextId = id+1;
	}
	
	@Override
	public void draw(Graphics2D g){
		if(coordinates.x - target.getCoordinates().x >= 0){  // 左向き
			super.draw(g);
		}else{  // 右向き
			drawFlippedHorizontal(g);
		}
	}


	@Override
	public void calc() {
		if( target == null ){
			return;
		}
		if( target.isDead() ){
			target = null;
		}
		move();
		attack();
	}
	
	private void move(){
		if( target == null ){
			return;
		}
		coordinates.moveTo(target.getCoordinates(), speed);
	}
	
	// 攻撃範囲内のターゲットに攻撃
	private void attack(){
		if( target == null ){
			return;
		}
		if( coordinates.distanceFrom(target.getCoordinates()) <= ATTACK_RANGE ){
			attackTo(target, 1);
			GameDataManager.getObjectManager().remove(this);
		}
	}

	@Override
	public void init() {
		ArrayList<GameObject> wizardList = GameDataManager.getObjectManager().findAllByTag(TagManager.PLAYER);
		if( target == null && wizardList.size() > 0 ){  // ターゲットを指定済みの場合はランダム選択しない
			wizardList = GameDataManager.getObjectManager().findAllByTag(TagManager.PLAYER);
			target = (Entity) wizardList.get(new Random().nextInt(wizardList.size()));
		}
		balloon = new Balloon(getBalloonCoord());
		//question = GameDataManager.getNextQuestion();
		question = GameDataManager.getQuestionByIndex(id);
		if (question != null){
			questionToBalloonText(question);
		}
		addChild(balloon);
	}
	
	@Override
	public void fallDown(){
		super.fallDown();
		DataManager.PointIncreace(1);
		if( Condition.playing_game_mode == 4 ){
			Client.sendMessage("fallDown " + id);
		}
	}
	
	public void receiveAttackMagic(String ans){
		System.out.println("answer:"+ question.answer +" reply:"+ans);
		if( isAnswerCorrect(ans) ){
			receiveDamage(1);
		}
	}
	
	public void receiveTurnMagic(String ans){
		System.out.println("answer:"+ question.answer +" reply:"+ans);
		if( isAnswerCorrect(ans) ){
			switchTarget();
			question = GameDataManager.getNextQuestion();
			questionToBalloonText(question);
		}
	}
	
	public void setBalloonText(String text){
		balloon.text = text;
	}
	
	private boolean isAnswerCorrect(String ans){
		return ans.equals(question.answer.trim());// 空白を取り除く
	}
	
	public void switchTarget(){
		ArrayList<GameObject> wizards = GameDataManager.getObjectManager().findAllByTag(TagManager.PLAYER);
		if( wizards.size() <= 1) return;
		if( wizards.get(0) == target) target = (Entity) wizards.get(1);
		else if( wizards.get(1) == target) target = (Entity) wizards.get(0);
	}
	
	private Coordinates getBalloonCoord(){
		return balloonCoordsToggle.toggle();
	}
	
	public Entity getTarget(){
		return target;
	}
	
	public void setTarget(Entity _target){
		target = _target;
	}
	
	// 向かってくる敵の中で最も近い者を返す
	public static Enemy searchNearestApproaching(GameObject target){
		ArrayList<GameObject> enemies = GameDataManager.getObjectManager().findAllByTag(TagManager.ENEMY);
		ArrayList<GameObject> approachingEnemies = new ArrayList<GameObject>();
		for( GameObject obj : enemies ){
			Enemy enm = (Enemy)obj;
			if( enm.getTarget() == target ){
				approachingEnemies.add(obj);
			}
		}
		return (Enemy) target.searchNearestInArray(approachingEnemies);
	}
	
	public static Enemy findById(long _id){
		ArrayList<GameObject> enemies = GameDataManager.gameObjectManager.findAllByTag(TagManager.ENEMY);
		for( GameObject enmObj : enemies){
			Enemy enm = (Enemy)enmObj;
			if ( enm.getId() == _id ){
				return enm;
			}	
		}
		return null;
	}
	
	public static long getNextId(){
		return nextId;
	}


	public long getId() {
		return id;
	}
	
	public Question getQuestion(){
		return question;
	}
	
	public void questionToBalloonText(Question question){
		balloon.setText(question.answer); // TODO
	}
}
