package game.telecommunication;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.PrintWriter;
import java.io.IOException;
//import java.io.*;

public class Server{
	private int port; // サーバの待ち受けポート
	private boolean [] online; //オンライン状態管理用配列
	private PrintWriter [] out; //データ送信用オブジェクト
	private Receiver [] receiver; //データ受信用オブジェクト
	private int playersRelation[];
	private boolean [] playing_game;
	private int [][] waiting;
	private Timer[] timer;
	private int [] enemy_id;

	//コンストラクタ
	public Server(int port) { //待ち受けポートを引数とする
		this.port = port; //待ち受けポートを渡す
		out = new PrintWriter [50]; //データ送信用オブジェクトを2クライアント分用意
		receiver = new Receiver [50]; //データ受信用オブジェクトを2クライアント分用意
		online = new boolean[50]; //オンライン状態管理用配列を用意
		playing_game = new boolean[50];
		playersRelation = new int[50];
		enemy_id = new int[50];
		waiting = new int[4][50];
		timer = new Timer[50];
		for(int i=0;i<50;i++){
			playersRelation[i]=-1;
			for(int j=0;j<4;j++)
				waiting[j][i]=-1;
		}
	}

	// データ受信用スレッド(内部クラス)
	class Receiver extends Thread {
		private InputStreamReader sisr; //受信データ用文字ストリーム
		private BufferedReader br; //文字ストリーム用のバッファ
		private int playerNo; //プレイヤを識別するための番号

		// 内部クラスReceiverのコンストラクタ
		Receiver (Socket socket, int playerNo){
			try{
				this.playerNo = playerNo; //プレイヤ番号を渡す
				sisr = new InputStreamReader(socket.getInputStream());
				br = new BufferedReader(sisr);
			} catch (IOException e) {
				System.err.println("データ受信時にエラーが発生しました: " + e);
			}
		}
		// 内部クラス Receiverのメソッド
		public void run(){
			try{
				while(true) {// データを受信し続ける
					String inputLine = br.readLine();//データを一行分読み込む
					if (inputLine != null){ //データを受信したら
						receiveMessage(inputLine,playerNo);
					}
				}
			} catch (IOException e){ // 接続が切れたとき
				System.err.println("Disconnected with player" + playerNo +".");
				online[playerNo] = false; //プレイヤの接続状態を更新する
				if(playing_game[playerNo]==true&&playerNo<playersRelation[playerNo]){
					timer[playerNo].cancel();
				}
				playing_game[playerNo] = false;		
				playersRelation[playerNo]=-1;		
			}
		}
	}
	
	public class EnemyEncounter extends TimerTask{
		int player1,player2;
		
		EnemyEncounter(int player1,int player2){
			this.player1=player1;
			this.player2=player2;
		}
		
		public void run(){
			Random rnd = new Random();
	        int ran = rnd.nextInt(2);
	        if(ran==0){
	        	forwardMessage("EnemyEncounter Forward "+enemy_id[player1],player1);
	        	forwardMessage("EnemyEncounter Backward "+enemy_id[player1],player2);
	        }else{
	        	forwardMessage("EnemyEncounter Forward "+enemy_id[player1],player2);
	        	forwardMessage("EnemyEncounter Backward "+enemy_id[player1],player1);
	        }
	        enemy_id[player1]++;
	        enemy_id[player2]++;
		}
	}

	// メソッド
	
	public void startEnemyEncounter(int player1,int player2){
		enemy_id[player1] = 0;
		timer[player1] = new Timer();
		timer[player1].schedule(new EnemyEncounter(player1,player2),0,10000);
	}
	
	public void matching(int level,int playerNo){
		if(waiting[level][0]!=-1){
			playersRelation[playerNo]=waiting[level][0];
			playersRelation[waiting[level][0]]=playerNo;
			delete_waiting(level,waiting[level][0]);
			forwardMessage("MatchingSuccess",playerNo);
			forwardMessage("MatchingSuccess",playersRelation[playerNo]);
			playing_game[playerNo] = true;
			playing_game[playersRelation[playerNo]]=true;
		}else{
			add_waiting(level,playerNo);
		}
	}
	
	public void delete_waiting(int level,int playerNo){
		int flag=0;
		int i=0;
		while(i<50&&flag==0){
			if(waiting[level][i]==playerNo){
				waiting[level][i]=-1;
				flag=1;
			}else{
				i++;
			}
		}
		for(i=0;i<49;i++)
			waiting[level][i]=waiting[level][i+1];
		while(i<49){
			waiting[level][i]=waiting[level][i+1];
			i++;
		}
		waiting[level][49]=-1;
	}
	
	public void add_waiting(int level,int playerNo){
		int flag=0;
		int i=0;
		while(i<50&&flag==0){
			if(waiting[level][i]==-1){
				waiting[level][i]=playerNo;
				flag=1;
			}
			i++;
		}
		if(flag==0)
			System.out.println("ERROR!!!!");
	}
	
	public void receiveMessage(String msg,int playerNo){
		String[] commandWords = msg.split(" ");
		System.out.println("Received \""+msg+"\" " + " from Player"+playerNo+".");
		if(commandWords[0].equals("Level")){
			int level = Integer.parseInt(commandWords[1]);
			matching(level,playerNo);
		}else if(commandWords[0].equals("turnEnemy")){//TurnEnemy EnemyID
		enemy_id[playersRelation[playerNo]]++;
		enemy_id[playerNo]++;
		forwardMessage(msg,playersRelation[playerNo]);
		}else if(commandWords[0].equals("fallDown")){
			forwardMessage(msg,playersRelation[playerNo]);
		}else if(commandWords[0].equals("GiveDamage")){//GiveDamage PlayerID
			forwardMessage(msg,playersRelation[playerNo]);
		}else if(commandWords[0].equals("StartEnemyEncounter")){
			if(playerNo<playersRelation[playerNo]){
				startEnemyEncounter(playerNo,playersRelation[playerNo]);
			}
		}else if(commandWords[0].equals("GameEnd")){
			if(playerNo<playersRelation[playerNo]){
				timer[playerNo].cancel();
			}
			playersRelation[playerNo]=-1;
			playing_game[playerNo] = false;
		}else if(commandWords[0].equals("Disconnect")){
			disconnect(Integer.parseInt(commandWords[1]),playerNo);
		}else if(commandWords[0].equals("Question")){
			if(playerNo<playersRelation[playerNo]){
				forwardMessage(msg,playersRelation[playerNo]);
				forwardMessage(msg,playerNo);
			}
		}
	}
	
	public void disconnect(int level,int playerNo){
		delete_waiting(level,playerNo);
		online[playerNo] = false; //プレイヤの接続状態を更新する
		System.out.println("Disconnected with player"+playerNo+".");
	}

	public void acceptClient(){ //クライアントの接続(サーバの起動)
		try {
			System.out.println("Started Server.");
			ServerSocket ss = new ServerSocket(port); //サーバソケットを用意
			while (true) {
				int no=getClientNum();
				Socket socket = ss.accept(); //新規接続を受け付ける
				receiver[no] = new Receiver(socket,no);
				online[no]=true;
				out [no] = new PrintWriter(socket.getOutputStream(), true);
				receiver[no].start();
			}
		} catch (Exception e) {
			System.err.println("ソケット作成時にエラーが発生しました: " + e);
		}
	}
	
	private int getClientNum(){
		int flag = 0;
		int i;
		for(i=0;i<50&&flag==0;i++){
			if(online[i]!=true)
				flag=1;
		}
		return i-1;
	}
	
	public void printStatus(){ //クライアント接続状態の確認
	}

	public void forwardMessage(String msg, int playerNo){ //操作情報の転送
		if(playerNo!=-1){
			out[playerNo].println(msg);//送信データをバッファに書き出す
			out[playerNo].flush();//送信データを送る
			System.out.println("Sended \""+msg+"\" " + " to Player"+playerNo); //テスト標準出力
		}
	}

	public static void main(String[] args){ //main
		Server server = new Server(10000); //待ち受けポート10000番でサーバオブジェクトを準備
		server.acceptClient(); //クライアント受け入れを開始
	}
}