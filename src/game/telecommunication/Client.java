package game.telecommunication;
import game.logic.gameobjects.Enemy;
import game.logic.gameobjects.EnemySpawnerMulti;
import game.logic.gameobjects.GameObject;
import game.logic.managers.GameDataManager;
import game.logic.managers.TagManager;

import java.net.*;
import java.util.ArrayList;
import java.io.*;

import menu.dialogs.MatchingDialog;
import menu.dialogs.data.Question;
import menu.dialogs.data.QuestionMaker;

public class Client {
	
	private static PrintWriter out;//データ送信用オブジェクト
	private static Receiver receiver; //データ受信用オブジェクト
	private static MatchingDialog matchingf;
	
	public static void initialize(MatchingDialog _matchingf){
		matchingf = _matchingf;
	}
	
//////////////////////////////外部から使用するメソッド///////////////////////////////////////////
	
	public static void connectServer(String ipAddress, int port){	// サーバに接続
		Socket socket = null;
		try {
			socket = new Socket(ipAddress, port); //サーバ(ipAddress, port)に接続
			out = new PrintWriter(socket.getOutputStream(), true); //データ送信用オブジェクトの用意
			receiver = new Receiver(socket); //受信用オブジェクトの準備
			receiver.start();//受信用オブジェクト(スレッド)起動
		} catch (UnknownHostException e) {
			System.err.println("ホストのIPアドレスが判定できません: " + e);
			System.exit(-1);
		} catch (IOException e) {
			System.err.println("サーバ接続時にエラーが発生しました: " + e);
			System.exit(-1);
		}
	}
	
	public static void sendMessage(String msg){	// サーバに操作情報を送信
		out.println(msg);//送信データをバッファに書き出す
		out.flush();//送信データを送る
		System.out.println("サーバにメッセージ " + msg + " を送信しました"); //テスト標準出力
	}
	
	public static void sendQuestion(Question[] q){
		StringBuffer str_bf = new StringBuffer();
		String str1,str2;
		str1 = ("Question "+q.length+" ");
		str_bf.append(str1);
		for(int i=0;i<q.length;i++){
			str1 = (q[i].question+",");
			str2 = (q[i].answer + ",");
			str_bf.append(str1);
			str_bf.append(str2);
		}
		str1 = str_bf.toString();
		sendMessage(str1);
	}
	
///////////////////////////////////////////////////////////////////////////////////////////
	
	public static void receiveQuestion(String msg){
		String[] commandWords = msg.split(" ");
		String str;
		StringBuffer strbf = new StringBuffer();
		int length = Integer.parseInt(commandWords[1]);
		for(int n = 2;n<commandWords.length;n++){
			strbf.append(commandWords[n]);
		}
		str=strbf.toString();
		String[] commandWords2 = str.split(",");
		Question[] q = new Question[length];
		int j=0;
		for(int i=0;i<length;i++){
			q[i] = new Question(commandWords2[j+1],commandWords2[j]);
			j=j+2;
		}
		QuestionMaker.question = q;
		matchingf.GameStart();
	}
	
	public static void receiveMessage(String msg){	// メッセージの処理
		System.out.println("サーバからメッセージ " + msg + " を受信しました"); //テスト用標準出力
		String[] commandWords = msg.split(" ");
		if(commandWords[0].equals("Damage")){	//相手がダメージを受けた場合
		}
		if(commandWords[0].equals("MatchingSuccess")){	//マッチング成功時
			matchingf.sendQuestion();
		}
		if(commandWords[0].equals("turnEnemy")){
			// Enemy を id で検索して反転
			long turnId = Long.parseLong(commandWords[1]);
			Enemy turnEnm = Enemy.findById(turnId);
			turnEnm.receiveTurnMagic(turnEnm.getQuestion().answer);
		}
		if(commandWords[0].equals("EnemyEncounter")){
			int enemyId = Integer.parseInt(commandWords[2]);
			if(commandWords[1].equals("Forward")){
				EnemySpawnerMulti spawner = (EnemySpawnerMulti)GameDataManager.getObjectManager().findByTag(TagManager.ENEMY_SPAWNER);
				spawner.spawnEnemyWithDirection(false, enemyId);
			}
			if(commandWords[1].equals("Backward")){
				EnemySpawnerMulti spawner = (EnemySpawnerMulti)GameDataManager.getObjectManager().findByTag(TagManager.ENEMY_SPAWNER);
				spawner.spawnEnemyWithDirection(true, enemyId);
			}
		}
		if(commandWords[0].equals("Question")){
			receiveQuestion(msg);
		}
		if(commandWords[0].equals("fallDown")){
			long id = Long.parseLong(commandWords[1]);
			Enemy.findById(id).fallDown();
		}
	}

	public static void main(String[] args) {
		// TODO 自動生成されたメソッド・スタブ
		Driver_Server driver = new Driver_Server();
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String input;
		driver.connectServer("133.34.213.152", 10000);
		try{
			while(true){ 
				System.out.print("文字を入力してください:");
				input = br.readLine();
				driver.sendMessage(input);
			}
		 }catch(IOException e){
			 System.out.println("Exception :" + e);   
		 }
	}
	
}
